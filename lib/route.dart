// import 'package:approval/screens/loginPage.dart';
import 'package:approval/routinig_objects.dart';
import 'package:approval/screens/bioLogin.dart';
import 'package:approval/screens/category.dart';
import 'package:approval/screens/mandate.dart';
import 'package:approval/screens/sumdetails.dart';
import 'package:approval/screens/transHistory.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'routing_constants.dart';
import 'screens/accountInfo.dart';
import 'screens/fullDetails.dart';
// import 'screens/intiRoute.dart';
import 'screens/landingPage.dart';
import 'screens/loooogin.dart';
import 'screens/accept.dart';
import 'screens/reject.dart';
import 'widgets/reject.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case LoginPageRoute:
      return MaterialPageRoute(builder: (context) => LoginPage());
      break;
    case HomePageRoute:
      return MaterialPageRoute(builder: (context) => HomePage());
      break;
    case SumDetailsRoute:
      final SumDetailsArguments arg = settings.arguments;

      return MaterialPageRoute(
          builder: (context) => SumDetails(
                description: arg.desc,
                itemCode: arg.code,
                username: arg.name,
              ));
      break;
    case FullDetailsRoute:
      final FullDetailsArguments arg = settings.arguments;

      return MaterialPageRoute(
          builder: (context) => FullDetails(
                arg.title,
                arg.data,
              ));
      break;
    case AccountInfoRoute:
      final arg = settings.arguments;

      return MaterialPageRoute(
          builder: (context) => AccInfoScreen(
                arg,
              ));
      break;
    case CategoryRoute:
      final arg = settings.arguments;

      return MaterialPageRoute(
          builder: (context) => Categories(
                arg,
              ));
      break;
    case MandateRoute:
      final arg = settings.arguments;
      return MaterialPageRoute(
          builder: (context) => MandateScreen(
                arg,
              ));

      break;
    case AcceptRoute:
      return MaterialPageRoute(builder: (context) => AcceptScreen());
      break;
    case RjectRoute:
      return MaterialPageRoute(builder: (context) => RejectScreen());
      break;
    case RjectWidgetRoute:
      // final arg = settings.arguments;
      return MaterialPageRoute(builder: (context) => RejectWidget());
      break;
    case HistoryRoute:
      // final arg = settings.arguments;
      return MaterialPageRoute(builder: (context) => TransactionHistory());
      break;
    case BiometricPageRoute:
      // final arg = settings.arguments;
      return MaterialPageRoute(builder: (context) => BiometricLogin());
      break;
    // case InitialRoute:
    //   final InitalRouteArgument arg = settings.arguments;
    //   return MaterialPageRoute(builder: (context) => InitialPage(arg));
    //   break;
    default:
      return MaterialPageRoute(builder: (context) => LoginPage());
  }
}
