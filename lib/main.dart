import 'package:approval/resources/provider.dart';
import 'package:approval/resources/sharedPreference.dart';
import 'package:approval/routing_constants.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/services.dart';

// import 'screens/loginPage.dart';
import 'route.dart';
import 'package:approval/resources/biometricLogin.dart';

import 'screens/pinPage.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  bool isBiometric = await isBiometricCheck();

  bool rememberMe = await isDataSaved();
  String name = await getusername();
  runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeState>.value(value: ThemeState()),
        ChangeNotifierProvider<User>.value(value: User()),
        ChangeNotifierProvider<RejectReasonList>.value(
            value: RejectReasonList()),
        ChangeNotifierProvider<RejectReasonProvider>.value(
            value: RejectReasonProvider()),
        ChangeNotifierProvider<TransactionDetails>.value(
            value: TransactionDetails()),
        ChangeNotifierProvider<SharedPreferenceData>.value(
            value: SharedPreferenceData()),
        ChangeNotifierProvider<Default>.value(value: Default()),
      ],
      child: MyApp(
        rememberMe: rememberMe,
        name: name,
        isBio: isBiometric,
      )));
}

class MyApp extends StatefulWidget {
  final bool rememberMe;
  final bool isBio;
  final String name;

  MyApp({this.rememberMe, this.name, this.isBio});
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    final savedname = Provider.of<User>(context, listen: false);

    if (widget.rememberMe) {
      savedname.setSavedName(widget.name);
      savedname.setBioMetric(widget.isBio);
    }
  }

  @override
  Widget build(BuildContext context) {
    // final themeData = Provider.of<ThemeState>(context);

    return MaterialApp(
      // initialRoute: widget.rememberMe ? BiometricPageRoute : LoginPageRoute,
      home: MyHomePage(),
      // routes: {
      //   '/': (context) => LoginPage(),
      //   '/home': (context) => HomePage(),
      //   '/sumDetail': (context) => SumDetails()
      // },
      onGenerateRoute: generateRoute,
      debugShowCheckedModeBanner: false,
      title: 'Central Approval',
      theme:
          // themeData.getTheme(),
          ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.light,
        primaryColor: Colors.blue,
        // primaryColor: Color(0xFF845EC2),
        // accentColor: Color(0xFFF9F871),
        backgroundColor: Color(0xFFFFC75F),
        buttonColor: Colors.blue[100],
        // buttonColor: Color(0xFFFF9671),

        // Define the default font family.
        fontFamily: 'gotham',

        // Define the default TextTheme. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          // title: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          // for text

          bodyText1: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.blue,
            // color: Color(0xFF845EC2),
            fontSize: 16,
            letterSpacing: 1,
          ),
          bodyText2: TextStyle(
            fontWeight: FontWeight.bold,
            color: Color(0XFF111E6C),
            // color: Color(0xFF845EC2),
            fontSize: 18,
            // letterSpacing: 1,
          ),
          // button: TextStyle(fon)

          headline2: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
          headline3: TextStyle(
            fontSize: 13.0,
          ),
          headline4: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w800,
            // color: Color(0XFF111E6C),
          ),
          headline5: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }
}
