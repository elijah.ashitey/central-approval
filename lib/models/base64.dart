// To parse this JSON data, do
//
//     final base64Api = base64ApiFromJson(jsonString);

import 'dart:convert';

List<Base64Api> base64ApiFromJson(String str) =>
    List<Base64Api>.from(json.decode(str).map((x) => Base64Api.fromJson(x)));

String base64ApiToJson(List<Base64Api> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Base64Api {
  String backImgBase64;
  String frontImgBase64;

  Base64Api({
    this.backImgBase64,
    this.frontImgBase64,
  });

  factory Base64Api.fromJson(Map<String, dynamic> json) => Base64Api(
        backImgBase64: json["backImgBase64"],
        frontImgBase64: json["frontImgBase64"],
      );

  Map<String, dynamic> toJson() => {
        "backImgBase64": backImgBase64,
        "frontImgBase64": frontImgBase64,
      };
}

/*
import 'dart:convert';

List<Base64Api> base64ApiFromJson(String str) =>
    List<Base64Api>.from(json.decode(str).map((x) => Base64Api.fromJson(x)));

String base64ApiToJson(List<Base64Api> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Base64Api {
  String backImgBase64;
  String frontImgBase64;

  Base64Api({
    this.backImgBase64,
    this.frontImgBase64,
  });

  factory Base64Api.fromJson(Map<String, dynamic> json) => Base64Api(
        backImgBase64: json["backImgBase64"],
        frontImgBase64: json["frontImgBase64"],
      );

  Map<String, dynamic> toJson() => {
        "backImgBase64": backImgBase64,
        "frontImgBase64": frontImgBase64,
      };
}
*/
