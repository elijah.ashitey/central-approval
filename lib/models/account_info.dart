// To parse this JSON data, do
//
//     final accountInfo = accountInfoFromJson(jsonString);

import 'dart:convert';

AccountInfo accountInfoFromJson(String str) =>
    AccountInfo.fromJson(json.decode(str));

String accountInfoToJson(AccountInfo data) => json.encode(data.toJson());

class AccountInfo {
  String accountName;
  String accountNumber;
  Balance balance;
  String currency;
  String customerName;
  String customerNumber;
  String primaryAccountEmail;
  String primaryAccountPhoneNumber;
  String product;

  AccountInfo({
    this.accountName,
    this.accountNumber,
    this.balance,
    this.currency,
    this.customerName,
    this.customerNumber,
    this.primaryAccountEmail,
    this.primaryAccountPhoneNumber,
    this.product,
  });

  factory AccountInfo.fromJson(Map<String, dynamic> json) => AccountInfo(
        accountName: json["accountName"],
        accountNumber: json["accountNumber"],
        balance: Balance.fromJson(json["balance"]),
        currency: json["currency"],
        customerName: json["customerName"],
        customerNumber: json["customerNumber"],
        primaryAccountEmail: json["primaryAccountEmail"],
        primaryAccountPhoneNumber: json["primaryAccountPhoneNumber"],
        product: json["product"],
      );

  Map<String, dynamic> toJson() => {
        "accountName": accountName,
        "accountNumber": accountNumber,
        "balance": balance.toJson(),
        "currency": currency,
        "customerName": customerName,
        "customerNumber": customerNumber,
        "primaryAccountEmail": primaryAccountEmail,
        "primaryAccountPhoneNumber": primaryAccountPhoneNumber,
        "product": product,
      };
}

class Balance {
  String accountName;
  String avBalance;
  String avBalanceSsh;
  String bkBalSsh;
  String bkBalance;
  String blkAmount;
  String brCode;
  String clearedBalance;
  String currency;
  String lastCdTransDate;
  String lastDbTransDate;
  String lienAmt;
  String noOfHolder;
  String odAmount;
  String product;
  String riskCode;
  String statusCode;
  String statusDesc;
  String unClearedBalance;

  Balance({
    this.accountName,
    this.avBalance,
    this.avBalanceSsh,
    this.bkBalSsh,
    this.bkBalance,
    this.blkAmount,
    this.brCode,
    this.clearedBalance,
    this.currency,
    this.lastCdTransDate,
    this.lastDbTransDate,
    this.lienAmt,
    this.noOfHolder,
    this.odAmount,
    this.product,
    this.riskCode,
    this.statusCode,
    this.statusDesc,
    this.unClearedBalance,
  });

  factory Balance.fromJson(Map<String, dynamic> json) => Balance(
        accountName: json["accountName"],
        avBalance: json["avBalance"],
        avBalanceSsh: json["avBalanceSsh"],
        bkBalSsh: json["bkBalSsh"],
        bkBalance: json["bkBalance"],
        blkAmount: json["blkAmount"],
        brCode: json["brCode"],
        clearedBalance: json["clearedBalance"],
        currency: json["currency"],
        lastCdTransDate: json["lastCDTransDate"],
        lastDbTransDate: json["lastDBTransDate"],
        lienAmt: json["lienAmt"],
        noOfHolder: json["noOfHolder"],
        odAmount: json["odAmount"],
        product: json["product"],
        riskCode: json["riskCode"],
        statusCode: json["statusCode"],
        statusDesc: json["statusDesc"],
        unClearedBalance: json["unClearedBalance"],
      );

  Map<String, dynamic> toJson() => {
        "accountName": accountName,
        "avBalance": avBalance,
        "avBalanceSsh": avBalanceSsh,
        "bkBalSsh": bkBalSsh,
        "bkBalance": bkBalance,
        "blkAmount": blkAmount,
        "brCode": brCode,
        "clearedBalance": clearedBalance,
        "currency": currency,
        "lastCDTransDate": lastCdTransDate,
        "lastDBTransDate": lastDbTransDate,
        "lienAmt": lienAmt,
        "noOfHolder": noOfHolder,
        "odAmount": odAmount,
        "product": product,
        "riskCode": riskCode,
        "statusCode": statusCode,
        "statusDesc": statusDesc,
        "unClearedBalance": unClearedBalance,
      };
}
