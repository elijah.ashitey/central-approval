// To parse this JSON data, do
//
//     final approve = approveFromJson(jsonString);

import 'dart:convert';

Approve approveFromJson(String str) => Approve.fromJson(json.decode(str));

String approveToJson(Approve data) => json.encode(data.toJson());

class Approve {
  String response;

  Approve({
    this.response,
  });

  factory Approve.fromJson(Map<String, dynamic> json) => Approve(
        response: json["response"],
      );

  Map<String, dynamic> toJson() => {
        "response": response,
      };
}
