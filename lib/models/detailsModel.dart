// To parse this JSON data, do
//
//     final info = infoFromJson(jsonString);

import 'dart:convert';

List<Info> infoFromJson(String str) =>
    List<Info>.from(json.decode(str).map((x) => Info.fromJson(x)));

String infoToJson(List<Info> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Info {
  String acctLink;
  String amount;
  String batchNo;
  String branchCode;
  String branchName;
  String currency;
  String documentRef;
  String flag;
  String itemCode;
  String name;
  String narration;
  String otherInfo;
  String postedBy;
  String postingDate;
  String time;
  String username;

  Info({
    this.acctLink,
    this.amount,
    this.batchNo,
    this.branchCode,
    this.branchName,
    this.currency,
    this.documentRef,
    this.flag,
    this.itemCode,
    this.name,
    this.narration,
    this.otherInfo,
    this.postedBy,
    this.postingDate,
    this.time,
    this.username,
  });

  factory Info.fromJson(Map<String, dynamic> json) => Info(
        acctLink: json["acct_Link"],
        amount: json["amount"],
        batchNo: json["batch_No"],
        branchCode: json["branch_Code"],
        branchName: json["branch_Name"],
        currency: json["currency"],
        documentRef: json["document_Ref"],
        flag: json["flag"],
        itemCode: json["item_Code"],
        name: json["name"],
        narration: json["narration"],
        otherInfo: json["other_Info"],
        postedBy: json["posted_By"],
        postingDate: json["posting_Date"],
        time: json["time"],
        username: json["username"],
      );

  Map<String, dynamic> toJson() => {
        "acct_Link": acctLink,
        "amount": amount,
        "batch_No": batchNo,
        "branch_Code": branchCode,
        "branch_Name": branchName,
        "currency": currency,
        "document_Ref": documentRef,
        "flag": flag,
        "item_Code": itemCode,
        "name": name,
        "narration": narration,
        "other_Info": otherInfo,
        "posted_By": postedBy,
        "posting_Date": postingDate,
        "time": time,
        "username": username,
      };
}
