// To parse this JSON data, do
//
//     final login = loginFromJson(jsonString);

import 'dart:convert';

Login loginFromJson(String str) => Login.fromJson(json.decode(str));

String loginToJson(Login data) => json.encode(data.toJson());

class Login {
  String brDesc;
  String pd;
  String globalAc;
  String globalBra;
  String globalDepartment;
  String globalEmpId;
  String globalHostname;
  String globalMachineip;
  String globalSubBra;
  String globalUsername;
  String loginCode;
  String mess;
  List<UserAccessList> userAccessList;

  Login({
    this.brDesc,
    this.pd,
    this.globalAc,
    this.globalBra,
    this.globalDepartment,
    this.globalEmpId,
    this.globalHostname,
    this.globalMachineip,
    this.globalSubBra,
    this.globalUsername,
    this.loginCode,
    this.mess,
    this.userAccessList,
  });

  factory Login.fromJson(Map<String, dynamic> json) => new Login(
        brDesc: json["BR_DESC"],
        pd: json["PD"],
        globalAc: json["global_AC"],
        globalBra: json["global_bra"],
        globalDepartment: json["global_department"],
        globalEmpId: json["global_emp_id"],
        globalHostname: json["global_hostname"],
        globalMachineip: json["global_machineip"],
        globalSubBra: json["global_sub_bra"],
        globalUsername: json["global_username"],
        loginCode: json["loginCode"],
        mess: json["mess"],
        userAccessList: new List<UserAccessList>.from(
            json["user_access_list"].map((x) => UserAccessList.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "BR_DESC": brDesc,
        "PD": pd,
        "global_AC": globalAc,
        "global_bra": globalBra,
        "global_department": globalDepartment,
        "global_emp_id": globalEmpId,
        "global_hostname": globalHostname,
        "global_machineip": globalMachineip,
        "global_sub_bra": globalSubBra,
        "global_username": globalUsername,
        "loginCode": loginCode,
        "mess": mess,
        "user_access_list":
            new List<dynamic>.from(userAccessList.map((x) => x.toJson())),
      };
}

List<UserAccessList> userAccessListFromJson(String str) =>
    List<UserAccessList>.from(
        json.decode(str).map((x) => UserAccessList.fromJson(x)));

String userAccessListToJson(List<UserAccessList> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class UserAccessList {
  String code;
  String description;
  String numApprovals;

  UserAccessList({
    this.code,
    this.description,
    this.numApprovals,
  });

  factory UserAccessList.fromJson(Map<String, dynamic> json) =>
      new UserAccessList(
        code: json["code"],
        description: json["description"],
        numApprovals: json["numApprovals"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "description": description,
        "numApprovals": numApprovals,
      };
}
