// To parse this JSON data, do
//
//     final pending = pendingFromJson(jsonString);

import 'dart:convert';

List<Pending> pendingFromJson(String str) =>
    List<Pending>.from(json.decode(str).map((x) => Pending.fromJson(x)));

String pendingToJson(List<Pending> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Pending {
  String code;
  String description;
  String numApprovals;

  Pending({
    this.code,
    this.description,
    this.numApprovals,
  });

  factory Pending.fromJson(Map<String, dynamic> json) => Pending(
        code: json["code"],
        description: json["description"],
        numApprovals: json["numApprovals"],
      );

  Map<String, dynamic> toJson() => {
        "code": code,
        "description": description,
        "numApprovals": numApprovals,
      };
}
