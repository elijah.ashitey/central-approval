// To parse this JSON data, do
//
//     final mandateModel = mandateModelFromJson(jsonString);

import 'dart:convert';

MandateModel mandateModelFromJson(String str) =>
    MandateModel.fromJson(json.decode(str));

String mandateModelToJson(MandateModel data) => json.encode(data.toJson());

class MandateModel {
  String responseCode;
  String message;
  Data data;

  MandateModel({
    this.responseCode,
    this.message,
    this.data,
  });

  factory MandateModel.fromJson(Map<String, dynamic> json) => MandateModel(
        responseCode: json["responseCode"],
        message: json["message"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "responseCode": responseCode,
        "message": message,
        "data": data.toJson(),
      };
}

class Data {
  String accountName;
  String accountNumber;
  String accountMandate;
  List<Relation> relations;

  Data({
    this.accountName,
    this.accountNumber,
    this.accountMandate,
    this.relations,
  });

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        accountName: json["accountName"],
        accountNumber: json["accountNumber"],
        accountMandate: json["accountMandate"],
        relations: List<Relation>.from(
            json["relations"].map((x) => Relation.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "accountName": accountName,
        "accountNumber": accountNumber,
        "accountMandate": accountMandate,
        "relations": List<dynamic>.from(relations.map((x) => x.toJson())),
      };
}

class Relation {
  String name;
  String panel;
  String signature;
  String photograph;

  Relation({
    this.name,
    this.panel,
    this.signature,
    this.photograph,
  });

  factory Relation.fromJson(Map<String, dynamic> json) => Relation(
        name: json["name"],
        panel: json["panel"],
        signature: json["signature"],
        photograph: json["photograph"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "panel": panel,
        "signature": signature,
        "photograph": photograph,
      };
}
