import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Fcard extends StatefulWidget {
  final String name;
  final String value;
  final double padding;
  final Color color;
  Fcard(this.name, this.value, this.padding, this.color);
  @override
  _FcardState createState() => _FcardState();
}

class _FcardState extends State<Fcard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(1),
      elevation: 0.0,
      child: Container(
        color: widget.color,
        // padding: EdgeInsets.all(15.0),
        child: Padding(
          padding: EdgeInsets.all(widget.padding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  child: Text(
                widget.name,
                style: Theme.of(context).textTheme.headline3,
              )),
              Expanded(
                  child: Text(
                widget.value,
                style: Theme.of(context).textTheme.headline2,
                textAlign: TextAlign.right,
              )),
            ],
          ),
        ),
      ),
    );
  }
}

class Fcard2 extends StatelessWidget {
  final String name;
  final String value;
  final double padding;
  final Color color;
  final Function function;
  Fcard2(this.name, this.value, this.padding, this.color, this.function);
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(1),
      elevation: 0.0,
      child: Container(
        color: color,
        // padding: EdgeInsets.all(15.0),
        child: Padding(
          padding: EdgeInsets.all(padding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  flex: 2,
                  child: Text(
                    name,
                    style: Theme.of(context).textTheme.headline3,
                  )),
              Expanded(
                  flex: 4,
                  child: Text(
                    value,
                    style: Theme.of(context).textTheme.headline2,
                    textAlign: TextAlign.right,
                  )),
              Expanded(
                  child: IconButton(
                icon: Icon(Icons.edit),
                onPressed: function,
              ))
            ],
          ),
        ),
      ),
    );
  }
}
