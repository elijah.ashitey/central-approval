import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Fcard extends StatefulWidget {
  final String name;
  final String value;
  final double padding;
  final Color color;
  final Color _textColor;
  Fcard(this.name, this.value, this.padding, this.color, this._textColor);
  @override
  _FcardState createState() => _FcardState();
}

class _FcardState extends State<Fcard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(1),
      elevation: 0.0,
      child: Container(
        color: widget.color,
        // padding: EdgeInsets.all(15.0),
        child: Padding(
          padding: EdgeInsets.all(widget.padding),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Text(
                    widget.name,
                    style: Theme.of(context).textTheme.headline3,
                  )),
              Expanded(
                  flex: 2,
                  child: Text(
                    widget.value,
                    textAlign: TextAlign.right,
                    style: Theme.of(context).textTheme.headline5,
                    /* TextStyle(
                      color: widget._textColor,
                      fontSize: 15,
                      // fontWeight: FontWeight.bold,
                      // fontFamily: 'gotham',
                    ),*/
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
