import 'package:flutter/material.dart';
import 'package:keyboard_avoider/keyboard_avoider.dart';
import 'package:provider/provider.dart';
import 'package:approval/resources/provider.dart';

class RejectWidget extends StatefulWidget {
  @override
  _RejectWidgetState createState() => _RejectWidgetState();
}

class _RejectWidgetState extends State<RejectWidget> {
  @override
  Widget build(BuildContext context) {
    final rejectreasonlist = Provider.of<RejectReasonList>(context);
    final rejectreason = Provider.of<RejectReasonProvider>(context);
    TextEditingController comment = new TextEditingController();
    // bool _other = false;
    return Scaffold(
      appBar: AppBar(
        title: Text("Comment"),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            // flex: 3,
            child: Container(
                // color: Colors.red,
                child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                if (rejectreason.getCode ==
                    rejectreasonlist.getReasons[index].code) {
                  return Card(
                    child: ListTile(
                      onTap: () {
                        rejectreason
                            .setCode(rejectreasonlist.getReasons[index].code);

                        if (rejectreason.getCode == 00) {
                        } else {
                          rejectreason.setComment(
                              rejectreasonlist.getReasons[index].reject);
                          rejectreason
                              .setCode(rejectreasonlist.getReasons[index].code);

                          Navigator.pop(context);
                        }
                      },
                      leading: CircleAvatar(
                        backgroundColor: Colors.blue[50],
                        child: Icon(Icons.chat),
                      ),
                      title: Text(rejectreasonlist.getReasons[index].reject),
                      trailing: Icon(Icons.check),
                    ),
                  );
                } else {
                  return Card(
                    child: ListTile(
                      onTap: () {
                        rejectreason
                            .setCode(rejectreasonlist.getReasons[index].code);

                        if (rejectreason.getCode == 00) {
                        } else {
                          rejectreason.setComment(
                              rejectreasonlist.getReasons[index].reject);

                          Navigator.pop(context);
                        }
                      },
                      leading: CircleAvatar(
                        backgroundColor: Colors.blue[50],
                        child: Icon(Icons.chat),
                      ),
                      title: Text(rejectreasonlist.getReasons[index].reject),
                    ),
                  );
                }
              },
              itemCount: 5,
            )),
          ),
          rejectreason.getCode == 00
              ? KeyboardAvoider(
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                      color: Colors.blue[50],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: Container(
                                  child: TextField(
                            maxLines: 3,
                            decoration: InputDecoration(
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                width: 2.0,
                                color: Colors.blue,
                              )),
                              labelText: 'comment here',
                            ),
                            controller: comment,
                          ))),
                          GestureDetector(
                              onTap: () {
                                if (comment.text.isEmpty) {
                                } else {
                                  rejectreason.setComment(comment.text);
                                  rejectreason.setCode(00);

                                  Navigator.pop(context);
                                }
                              },
                              child: CircleAvatar(
                                child: Icon(Icons.send),
                              ))
                        ],
                      ),
                    ),
                  ),
                )
              : Container(),
        ],
      ),
      /* bottomNavigationBar: KeyboardAvoider(
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Row(
            children: <Widget>[
              Expanded(
                  child: Container(
                      child: TextField(
                decoration: InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                    width: 2.0,
                    color: Colors.blue,
                  )),
                  labelText: 'comment here',
                ),
                controller: comment,
              ))),
              GestureDetector(
                  child: CircleAvatar(
                child: Icon(Icons.send),
              ))
            ],
          ),
        ),
      ),*/

      /* Row(
        children: <Widget>[
          TextField(
            maxLines: 3,
            decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                width: 2.0,
                color: Colors.white,
              )),
              labelText: 'comment here',
            ),
            controller: comment,
          ),
          // GestureDetector(
          //     child: CircleAvatar(
          //   child: Icon(Icons.send),
          // ))
        ],
      ),*/
    );
  }
}
