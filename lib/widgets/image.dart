import 'dart:convert';
import 'dart:typed_data';

import 'package:approval/models/base64.dart';
import 'package:approval/resources/apiCalls.dart';
import 'package:approval/widgets/retry.dart';
import 'package:flutter/material.dart';

import 'loader.dart';

class ImgScreen extends StatefulWidget {
  final String batchNo;
  ImgScreen(
    this.batchNo,
  );
  @override
  _ImgScreenState createState() => _ImgScreenState();
}

class _ImgScreenState extends State<ImgScreen> {
  Future<Base64Api> data;
  @override
  void initState() {
    super.initState();
    data = readBase64(widget.batchNo);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(title: Text('Image')),
        // backgroundColor: Colors.white,
        body: FutureBuilder(
            future: data,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.none ||
                  snapshot.connectionState == ConnectionState.active ||
                  snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: loading());
              } else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return retry(() {
                    setState(() {
                      data = readBase64(widget.batchNo);
                    });
                  }, "No data Found");
                } else if (snapshot.data == null) {
                  return Center(
                    child: Text("Null"),
                  );
                } else if (snapshot.data != null) {
                  Base64Api resp = snapshot.data;

                  if (resp == null) {
                    return Center(
                      child: Text("No data Found"),
                    );
                  } else if (resp.backImgBase64 == null ||
                      resp.frontImgBase64 == null ||
                      resp.backImgBase64 == "" ||
                      resp.frontImgBase64 == "") {
                    return Center(
                      child: Text("No image found"),
                    );
                  } else {
                    if (resp.backImgBase64.length < 30 ||
                        resp.frontImgBase64.length < 30) {
                      return Center(
                        child: Text("No image "),
                      );
                    } else {
                      String _base64f =
                          resp.backImgBase64.replaceAll(new RegExp(r"\s+"), "");
                      String _base64b = resp.frontImgBase64
                          .replaceAll(new RegExp(r"\s+"), "");
                      Uint8List bytesf = base64.decode(_base64f);
                      Uint8List bytesb = base64.decode(_base64b);
                      return GestureDetector(
                        // onTap: () {
                        //   showDialog(
                        //       context: context,
                        //       child: view(context, view)
                        // },
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                color: Colors.white,
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          showDialog(
                                              context: context,
                                              child: view(
                                                context,
                                                Center(
                                                    child: Image.memory(
                                                  bytesf,
                                                  fit: BoxFit.cover,
                                                )),
                                              ));
                                        },
                                        child: Container(
                                          // color: Colors.blue,
                                          child: Center(
                                              child: Image.memory(
                                            bytesf,
                                            fit: BoxFit.cover,
                                          )),
                                        ),
                                      ),
                                    ),
                                    Text(
                                      "Front of cheque",
                                      style:
                                          Theme.of(context).textTheme.headline3,
                                    )
                                  ],
                                ),
                              ),
                            ),
                            VerticalDivider(),
                            Expanded(
                              child: Container(
                                color: Colors.white,
                                child: Column(
                                  children: <Widget>[
                                    Expanded(
                                      child: GestureDetector(
                                        onTap: () {
                                          showDialog(
                                              context: context,
                                              child: view(
                                                context,
                                                Center(
                                                    child: Image.memory(
                                                  bytesb,
                                                  fit: BoxFit.cover,
                                                )),
                                              ));
                                        },
                                        child: Container(
                                          // width: 50,
                                          // height: 10,
                                          // color: Colors.blue,
                                          child: Image.memory(
                                            bytesb,
                                            fit: BoxFit.fill,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Icon(
                                          Icons.image,
                                          size: 12,
                                          color: Colors.blue,
                                        ),
                                        Text(
                                          "Back of cheque",
                                          style: Theme.of(context)
                                              .textTheme
                                              .headline3,
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                  }
                } else {
                  return Center(
                    child: Text("error from no where"),
                  );
                }
              } else {
                return Center(
                  child: Text("without snapshot error"),
                );
              }
            }));
  }
}

view(context, view) {
  return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
            height: MediaQuery.of(context).size.height * 0.5,
            width: MediaQuery.of(context).size.width,
            child: view),
      ]);
}
