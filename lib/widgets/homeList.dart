import 'package:flutter/material.dart';
import 'package:polygon_clipper/polygon_clipper.dart';

class HomeList extends StatelessWidget {
  final String number;
  final String description;
  final Function function;
  HomeList(this.number, this.description, this.function);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          onTap: function,
          leading: ClipPolygon(
            child: Container(
                // width: MediaQuery.of(context).size.width * 0.1,
                // height: MediaQuery.of(context).size.height * 0.1,
                decoration: BoxDecoration(
                  color: Colors.white,
                  // gradient: LinearGradient(
                  //   begin: Alignment.topLeft,
                  //   end: Alignment.bottomRight,
                  //   colors: [
                  //     Colors.grey[200],
                  //     Colors.grey[800],
                  //   ],
                  // ),
                ),
                child: Center(
                  child: Text(
                    number,
                    style: TextStyle(
                      fontSize: 12,
                      color: Color(0XFF111E6C),
                    ),
                  ),
                )),
            boxShadows: [
              PolygonBoxShadow(color: Colors.black, elevation: 5.0),
            ],
            sides: 6,
            borderRadius: 1.0,
          ),
          title: Text(
            description,
            style: TextStyle(fontSize: 15, color: Colors.black45),
          ),
        ),
        Divider()
      ],
    );
  }
}
