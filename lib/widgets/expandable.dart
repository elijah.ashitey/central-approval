import 'package:approval/models/account_info.dart';
import 'package:approval/resources/amtFormat.dart';
import 'package:approval/resources/provider.dart';
import 'package:approval/widgets/sumDetailList.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'heading.dart';

class AccInfo extends StatelessWidget {
  final AccountInfo resp;
  const AccInfo(this.resp);

  @override
  Widget build(BuildContext context) {
    final res = Provider.of<TransactionDetails>(context);
// var resp = data
    // Color _brown = Color.fromRGBO(52, 8, 11, 1);
    // Color _orange = Color.fromRGBO(255, 158, 110, 0.5);
    // Color _orange1 = Color.fromRGBO(255, 158, 110, 1);
    return ExpandableNotifier(
        child: Card(
      // elevation: 13,
      // clipBehavior: Clip.antiAlias,
      child: Column(
        children: <Widget>[
          Container(
            // color: Colors.grey[100],
            // padding: EdgeInsets.all(15.0),
            child: Padding(
              padding: const EdgeInsets.all(7.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Heading("Account Details"),
                  // Divider(
                  //   indent: 100,
                  //   endIndent: 100,
                  //   height: 5,
                  // ),

                  Fcard(
                    "Name",
                    res.getDetail.name,
                    7,
                    Colors.white,
                    Colors.white,
                  ),
                  Divider(),
                  Fcard(
                    "Branch Name",
                    res.getDetail.branchName,
                    7,
                    Colors.white,
                    Colors.white,
                  ),
                ],
              ),
            ),
          ),
          Divider(),
          ScrollOnExpand(
            scrollOnExpand: true,
            scrollOnCollapse: false,
            child: ExpandablePanel(
              theme: const ExpandableThemeData(
                headerAlignment: ExpandablePanelHeaderAlignment.bottom,
                tapBodyToCollapse: true,
              ),
              header: Fcard(
                "Account Number",
                res.getDetail.acctLink,
                7,
                Colors.white,
                Colors.white,
              ),
              /*Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(child: Text("Account Info")),
              ),*/
              // collapsed:
              /* Container(
                color: Color.fromRGBO(52, 8, 11, 0.1),
                padding: EdgeInsets.all(10),
                child: Row(
                  children: <Widget>[
                    Text(
                      'Repayment Date',
                      softWrap: true,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: Colors.black87,
                      ),
                    ),
                    Expanded(
                      child: Text(
                        date,
                        softWrap: true,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        textAlign: TextAlign.end,
                        style: TextStyle(
                            // color: UIData.appPrimaryColor,
                            ),
                      ),
                    ),
                  ],
                ),
              ),*/
              expanded: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Divider(
                    height: 5,
                  ),
                  // Fcard(
                  //   "Account Number",
                  //   resp.accountNumber,
                  //   7,
                  //   Colors.white,
                  //   Colors.white,
                  // ),
                  // Divider(),
                  // Fcard(
                  //   "Account Name",
                  //   resp.accountName,
                  //   7,
                  //   Colors.white,
                  //   Colors.white,
                  // ),
                  // Divider(),
                  Fcard(
                    "Product",
                    resp.balance.product,
                    7,
                    Colors.white,
                    Colors.white,
                  ),
                  Divider(),
                  Fcard(
                    "Risk Code",
                    resp.balance.riskCode,
                    7,
                    Colors.white,
                    Colors.white,
                  ),
                  Divider(),
                  Fcard(
                    "Account Branch ",
                    resp.balance.brCode,
                    7,
                    Colors.white,
                    Colors.white,
                  ),
                  Divider(),
                  Fcard(
                    "Book Amount",
                    resp.currency + " " + formatAmounts(resp.balance.bkBalance),
                    7,
                    Colors.white,
                    Colors.white,
                  ),
                  Divider(),
                  Fcard(
                    "Available Balance",
                    resp.currency + " " + formatAmounts(resp.balance.avBalance),
                    7,
                    Colors.white,
                    Colors.white,
                  ),
                ],
              ),
              builder: (_, collapsed, expanded) {
                return Padding(
                  padding: EdgeInsets.only(left: 0, right: 0, bottom: 0),
                  child: Expandable(
                    collapsed: collapsed,
                    expanded: expanded,
                    theme: const ExpandableThemeData(crossFadePoint: 0),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    ));
  }
}

Widget loandet(String desc, String data) {
  Color _brown = Color.fromRGBO(52, 8, 11, 1);

  return Padding(
    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
    child: Row(
      children: <Widget>[
        Text(
          desc,
          softWrap: true,
          overflow: TextOverflow.fade,
          style: TextStyle(
            color: _brown,
          ),
        ),
        Expanded(
          child: Text(
            data,
            softWrap: true,
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
            textAlign: TextAlign.end,
            style: TextStyle(
              color: Colors.blue,
            ),
          ),
        ),
      ],
    ),
  );
}
