import 'package:flutter/material.dart';

retry(Function function, text) {
  return Center(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
        Text(
          text,
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        RaisedButton(
          color: Colors.blue,
          onPressed: function,
          child: Text(
            "Retry",
            style: TextStyle(color: Colors.white),
          ),
        )
      ]));
}

errorretry(Function function) {
  return Center(
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
        Text(
          "An Error Occured ",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        RaisedButton(
          color: Colors.blue,
          onPressed: function,
          child: Text(
            "Retry",
            style: TextStyle(color: Colors.white),
          ),
        )
      ]));
}
