import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

import 'charts/barChart.dart';
import 'charts/pieChart.dart';
import 'charts/model.dart';
// import 'TransFullDetails.dart';
// import 'transChart.dart';

class NewTransCart extends StatefulWidget {
  // final String accNum;
  // final MbTransDetail details;

  // NewTransCart(this.details);
  @override
  _NewTransCartState createState() => _NewTransCartState();
}

class _NewTransCartState extends State<NewTransCart> {
  // Future<TransactionModel> data;

  static List<charts.Series<LinearSales2, int>> _createSampleData() {
    final data = [
      LinearSales2("Elijah", 100, Colors.redAccent, 4),
      LinearSales2("AShitey", 75, Colors.amber, 3),
      LinearSales2("Lord", 25, Colors.pinkAccent, 2),
      LinearSales2("Ruby", 50, Colors.lightGreenAccent, 1),
    ];

    return [
      charts.Series<LinearSales2, int>(
        id: 'Trial',
        domainFn: (LinearSales2 sales, _) => sales.dont,
        measureFn: (LinearSales2 sales, _) => sales.sales,
        colorFn: (LinearSales2 sales, _) =>
            charts.ColorUtil.fromDartColor(sales.color),
        data: data,
        labelAccessorFn: (LinearSales2 sales, _) => sales.year,
      )
    ];
  }

  static List<charts.Series<OrdinalSales, String>> _createSampleData2() {
    final data = [
      new OrdinalSales('2014', 5),
      new OrdinalSales('2015', 25),
      new OrdinalSales('2016', 100),
      new OrdinalSales('2017', 75),
    ];

    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }

  @override
  Widget build(BuildContext context) {
    // Color _brown = Color.fromRGBO(52, 8, 11, 1);
    Color _iconColor = Color.fromRGBO(244, 128, 71, 1);

//  Color _orang =   Color.fromRGBO(255, 158, 110, 1);
    // Color _orang =  Colors.white;

    return Scaffold(
      /*appBar: AppBar(
        leading: IconButton(
          color: _iconColor,
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Icon(Icons.arrow_back_ios),
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        title: Text(
          "Transaction Details ",
          style: TextStyle(color: _brown, fontWeight: FontWeight.bold),
        ),
      ),*/
      backgroundColor: _iconColor,
      body: Column(
        children: <Widget>[
          Expanded(
            child: ClipRRect(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(
                  30.0,
                ),
                topRight: Radius.circular(
                  30.0,
                ),
              ),
              child: Container(
                decoration: BoxDecoration(
                  // image: new DecorationImage(
                  //     image: new AssetImage("assets/images/bkk.png"),
                  //     fit: BoxFit.cover),
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(
                      30.0,
                    ),
                    topRight: Radius.circular(
                      30.0,
                    ),
                  ),
                ),
                child: ListView(
                  children: <Widget>[
                    Text("Elijah"),
                    DonutPieChart(_createSampleData()),
                    // SimpleBarChart(_createSampleData2()),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
