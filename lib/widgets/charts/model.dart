import 'dart:ui';

class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}

class LinearSales2 {
  final String year;
  final int sales;
  final Color color;
  final int dont;
  LinearSales2(this.year, this.sales, this.color, this.dont);
}
