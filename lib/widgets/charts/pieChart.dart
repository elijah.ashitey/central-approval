import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

import 'model.dart';

class DonutPieChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  DonutPieChart(this.seriesList, {this.animate});

  /// Creates a [PieChart] with sample data and no transition.
  factory DonutPieChart.withSampleData() {
    return DonutPieChart(
      _createSampleData(),
      // Disable animations for image tests.
      animate: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.2,
      decoration: BoxDecoration(color: Colors.transparent
          // image: new DecorationImage(
          //     image: new AssetImage("assets/images/b.png"), fit: BoxFit.cover),
          ),
      child: charts.PieChart(
        seriesList,
        animate: animate,
        animationDuration: Duration(seconds: 1),
        behaviors: [
          charts.DatumLegend(
              outsideJustification: charts.OutsideJustification.end,
              horizontalFirst: true,
              desiredMaxRows: 2,
              cellPadding: EdgeInsets.all(10),
              entryTextStyle: charts.TextStyleSpec(
                color: charts.MaterialPalette.purple.shadeDefault,
                fontSize: 18,
              ))
        ],

        // Configure the width of the pie slices to 60px. The remaining space in
        // the chart will be left as a hole in the center.
        defaultRenderer:
            charts.ArcRendererConfig(arcWidth: 100, arcRendererDecorators: [
          charts.ArcLabelDecorator(
              insideLabelStyleSpec: charts.TextStyleSpec(
                color: charts.ColorUtil.fromDartColor(Colors.black),
                fontSize: 18,
              ),
              labelPosition: charts.ArcLabelPosition.auto)
        ]),
      ),
    );
  }

  /// Create one series with sample hard coded data.
  static List<charts.Series<LinearSales, int>> _createSampleData() {
    final data = [
      LinearSales(0, 100),
      LinearSales(1, 75),
      LinearSales(2, 25),
      LinearSales(3, 5),
    ];

    return [
      charts.Series<LinearSales, int>(
        id: 'Sales',
        domainFn: (LinearSales sales, _) => sales.year,
        measureFn: (LinearSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }
}
/*
/// Sample linear data type.
class LinearSales {
  final int year;
  final int sales;

  LinearSales(this.year, this.sales);
}
*/
