import 'package:flutter/material.dart';

approveInfo(title, number, type) {
  Color _color;
  if (type == 1) {
    _color = Colors.blue[100];
  } else if (type == 2) {
    _color = Colors.green[100];
  } else if (type == 3) {
    _color = Colors.red[100];
  } else {}
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      // SizedBox(
      //   height: 50,
      // ),
      Text(
        title,
        style: TextStyle(fontSize: 20, color: Colors.grey),
      ),
      Padding(
        padding: const EdgeInsets.only(right: 10.0),
        child: Text(
          number,
          style: TextStyle(
              fontSize: 30, fontWeight: FontWeight.bold, color: _color),
        ),
      ),
    ],
  );
}

approvalStatusCard(BuildContext context, type) {
  Color _color;
  String _text;
  if (type == 1) {
    _color = Colors.blue[100];
    _text = "Pending";
  } else if (type == 2) {
    _color = Colors.green[100];
    _text = "Approved";
  } else if (type == 3) {
    _color = Colors.red[100];
    _text = "Rejected";
  } else {
    _color = Colors.white;
  }
  return Container(
    height: MediaQuery.of(context).size.height * 0.08,
    width: MediaQuery.of(context).size.width * 0.3,
    // color: Colors.yellow,
    child: Stack(
      children: <Widget>[
        Align(
          alignment: Alignment.bottomCenter,
          child: Card(
            elevation: 3,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.04,
              // width:
              //     MediaQuery.of(context).size.width * 0.1,
              color: Colors.grey[100],
              child: Center(
                  child: Text(
                _text,
                style: TextStyle(
                    color: Colors.blueGrey,
                    fontFamily: 'gotham',
                    fontSize: 13,
                    fontWeight: FontWeight.w700),
              )),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: Card(
            elevation: 3,
            child: Container(
              height: MediaQuery.of(context).size.height * 0.03,
              // width: MediaQuery.of(context).size.width * 0.08,
              color: _color,
              child: Center(
                  child: Text(
                "1000",
                style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'gotham',
                    fontSize: 10,
                    fontWeight: FontWeight.bold),
              )),
            ),
          ),
        )
      ],
    ),
  );
}

// status(){
//   return Container(
//     ch
//   )
// }
