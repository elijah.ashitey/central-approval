import 'models/detailsModel.dart';

class SumDetailsArguments {
  final String desc;
  final String code;
  final String name;
  SumDetailsArguments(this.desc, this.code, this.name);
}

class FullDetailsArguments {
  final String title;
  final Info data;

  FullDetailsArguments(this.title, this.data);
}

class InitalRouteArgument {
  final bool rememberMe;
  final String name;

  InitalRouteArgument(this.rememberMe, this.name);
}
