const String LoginPageRoute = '/';
const String BiometricPageRoute = '/bioLogin';
const String HomePageRoute = '/home';
const String SumDetailsRoute = '/sumDetails';
const String FullDetailsRoute = '/fullDetails';
const String AccountInfoRoute = '/accountInfo';
const String MandateRoute = '/mandate';
const String AcceptRoute = '/accept';
const String RjectRoute = '/reject';
const String RjectWidgetRoute = '/rejectwidget';
const String HistoryRoute = '/transactionHistory';
const String CategoryRoute = '/Category';
const String InitialRoute = '/initRoute';
