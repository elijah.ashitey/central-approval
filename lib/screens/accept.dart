import 'package:approval/resources/apiCalls.dart';
import 'package:approval/resources/connectionChecker.dart';
import 'package:approval/resources/provider.dart';
import 'package:approval/widgets/card.dart';
import 'package:approval/widgets/dialogs.dart';
import 'package:approval/widgets/loader.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter/services.dart';

import '../routing_constants.dart';

class AcceptScreen extends StatefulWidget {
  @override
  _AcceptScreenState createState() => _AcceptScreenState();
}

class _AcceptScreenState extends State<AcceptScreen> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    final res = Provider.of<TransactionDetails>(context);
    final user = Provider.of<User>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("CONFIRM APPROVAL"),
        elevation: 0,
        actions: <Widget>[
          IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(
              Icons.close,
              color: Colors.white,
            ),
          )
        ],
      ),
      body: _isLoading
          ? Center(
              child: loading(),
            )
          : Column(
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListView(
                      children: <Widget>[
                        Fcard(
                          "Account Number",
                          res.getDetail.acctLink,
                          20,
                          Colors.transparent,
                        ),
                        Fcard(
                          "Account Name",
                          res.getDetail.name,
                          20,
                          Colors.grey[200],
                        ),
                        Fcard(
                          "Account Branch ",
                          res.getDetail.branchName,
                          20,
                          Colors.transparent,
                        ),
                        Fcard(
                          "Amount",
                          res.getDetail.currency + " " + res.getDetail.amount,
                          20,
                          Colors.grey[200],
                        ),
                        Fcard(
                          "Cheque No",
                          res.getDetail.documentRef,
                          20,
                          Colors.transparent,
                        ),
                        Fcard(
                          "Narration",
                          res.getDetail.narration,
                          20,
                          Colors.grey[200],
                        ),
                        Fcard(
                          "Posted By",
                          res.getDetail.postedBy,
                          20,
                          Colors.transparent,
                        ),
                        Fcard(
                          "Posting Date",
                          res.getDetail.postingDate,
                          20,
                          Colors.grey[200],
                        ),
                        Fcard(
                          "Posting time",
                          res.getDetail.time,
                          20,
                          Colors.transparent,
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  height: 45,
                  width: MediaQuery.of(context).size.width / 1.2,
                  decoration: BoxDecoration(
                    // gradient: getCustomgradient(),
                    color: Colors.blue,
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
                  child: FlatButton(
                    onPressed: () async {
                      try {
                        setState(() {
                          _isLoading = true;
                        });
                        final checkinternet = await internetCheck();
                        if (checkinternet) {
                          final username = user.getuser.globalUsername;

                          final resp = await approvAPI(
                              username,
                              res.getDetail.otherInfo,
                              'Y',
                              "",
                              res.getDetail.batchNo);
                          print(username);
                          print(res.getDetail.otherInfo);
                          print(res.getDetail.batchNo);
                          if (resp.response != null) {
                            err(
                              context,
                              resp.response,
                            );
                          } else {
                            success(
                                context,
                                res.getDetail.batchNo +
                                    "has been approved successfully", () {
                              Navigator.of(context).pushNamed(HomePageRoute);
                            });
                          }
                        } else {
                          info(context, "No Internet Access");
                        }
                      } on PlatformException catch (e) {
                        print("Error:${e.message}");
                        err(context, e.message.toString());
                      } finally {
                        setState(() {
                          _isLoading = false;
                        });
                      }
                    },

                    child: Text('Confirm',
                        style: TextStyle(color: Colors.white, fontSize: 22.0)),
                    // textColor: Colors.red,
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            color: Colors.white,
                            width: 1,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.circular(6)),
                  ),
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
    );
  }
}
