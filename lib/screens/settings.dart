import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  bool _value = false;

  void onToggle(bool value) {
    setState(() {
      _value = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Text(""),
        title: Text(
          "Settings",
        ),
        centerTitle: true,
        elevation: 0,
      ),
      backgroundColor: Colors.grey[300],
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(child: Text("Account")),
          ),
          Card(
            child: Column(
              children: <Widget>[
                ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Colors.red[300],
                    child: Icon(
                      Icons.person,
                      color: Colors.white,
                    ),
                  ),
                  title: Text("Change Username"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                line(),
                ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Colors.green[300],
                    child: Icon(
                      Icons.vpn_key,
                      color: Colors.white,
                    ),
                  ),
                  title: Text("Change Password"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
                line(),
                ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Colors.amber[300],
                    child: Icon(
                      Icons.person_pin,
                      color: Colors.white,
                    ),
                  ),
                  title: Text("Change Account Info"),
                  trailing: Icon(Icons.arrow_forward_ios),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(child: Text("Quick Setting")),
          ),
          Card(
            child: Column(
              children: <Widget>[
                ListTile(
                    leading: CircleAvatar(
                      backgroundColor: Colors.orange[300],
                      child: Icon(
                        Icons.notifications,
                        color: Colors.white,
                      ),
                    ),
                    title: Text("Notification"),
                    trailing: Switch(
//                    inactiveThumbColor: _orang,
                      activeColor: Colors.blue,
                      value: _value,
                      onChanged: (bool val) {
                        onToggle(val);
                      },
                    )),
                line(),
                ListTile(
                    leading: CircleAvatar(
                      backgroundColor: Colors.teal[300],
                      child: Icon(
                        Icons.fingerprint,
                        color: Colors.white,
                      ),
                    ),
                    title: Text("Biometric"),
                    trailing: Switch(
//                    inactiveThumbColor: _orang,
                      activeColor: Colors.blue,
                      value: _value,
                      onChanged: (bool val) {
                        onToggle(val);
                      },
                    )),
                line(),
                ListTile(
                    leading: CircleAvatar(
                      backgroundColor: Colors.purple[300],
                      child: Icon(
                        Icons.save,
                        color: Colors.white,
                      ),
                    ),
                    title: Text("Auto save Username"),
                    trailing: Switch(
//                    inactiveThumbColor: _orang,
                      activeColor: Colors.blue,
                      value: _value,
                      onChanged: (bool val) {
                        onToggle(val);
                      },
                    )),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(child: Text("Quick Setting")),
          ),
          Card(
            child: Column(
              children: <Widget>[
                ListTile(
                    leading: CircleAvatar(
                      backgroundColor: Colors.cyan[300],
                      child: Icon(
                        Icons.notifications,
                        color: Colors.white,
                      ),
                    ),
                    title: Text("Notification"),
                    trailing: Switch(
//                    inactiveThumbColor: _orang,
                      activeColor: Colors.blue,
                      value: _value,
                      onChanged: (bool val) {
                        onToggle(val);
                      },
                    )),
                line(),
                ListTile(
                    leading: CircleAvatar(
                      backgroundColor: Colors.brown[300],
                      child: Icon(
                        Icons.fingerprint,
                        color: Colors.white,
                      ),
                    ),
                    title: Text("Biometric"),
                    trailing: Switch(
//                    inactiveThumbColor: _orang,
                      activeColor: Colors.blue,
                      value: _value,
                      onChanged: (bool val) {
                        onToggle(val);
                      },
                    )),
                line(),
                ListTile(
                    leading: CircleAvatar(
                      backgroundColor: Colors.lime[300],
                      child: Icon(
                        Icons.save,
                        color: Colors.white,
                      ),
                    ),
                    title: Text("Auto save Username"),
                    trailing: Switch(
//                    inactiveThumbColor: _orang,
                      activeColor: Colors.blue,
                      value: _value,
                      onChanged: (bool val) {
                        onToggle(val);
                      },
                    )),
              ],
            ),
          )
        ],
      ),
    );
  }
}

line() {
  return Container(
    color: Colors.grey[200],
    // width: MediaQuery.of(context).size.width,
    height: 1,
  );
}
