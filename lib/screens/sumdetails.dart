import 'package:approval/models/detailsModel.dart';
import 'package:approval/resources/apiCalls.dart';
import 'package:approval/resources/provider.dart';
import 'package:approval/routing_constants.dart';
import 'package:approval/routinig_objects.dart';
import 'package:approval/widgets/loader.dart';
import 'package:approval/widgets/retry.dart';
import 'package:approval/widgets/sumDetailList.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SumDetails extends StatefulWidget {
  final String itemCode;
  final String username;
  final String description;
  SumDetails({this.itemCode, this.username, this.description});
  @override
  _SumDetailsState createState() => _SumDetailsState();
}

class _SumDetailsState extends State<SumDetails> {
  Future<List<Info>> data;
  String value;
  @override
  void initState() {
    super.initState();
    print(widget.itemCode);
    print(widget.username);
    print(widget.description);
    data = transDetails(widget.itemCode, widget.username);
  }

  List<DropdownMenuItem> listDrop = [];

  void list() {
    listDrop = [];
    listDrop.add(DropdownMenuItem(
      child: Container(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "All",
            style: TextStyle(fontSize: 13),
          ),
        ],
      )),
      value: "25",
    ));
    listDrop.add(DropdownMenuItem(
      child: Container(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // Divider(),
          Text(
            "Withdrawals",
            style: TextStyle(fontSize: 13),
          ),
        ],
      )),
      value: "50",
    ));
    listDrop.add(DropdownMenuItem(
      child: Container(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // Divider(),
          Text(
            "Deposits",
            style: TextStyle(fontSize: 13),
          ),
        ],
      )),
      value: "100",
    ));
  }

  @override
  Widget build(BuildContext context) {
    final trans = Provider.of<TransactionDetails>(context);
    list();
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.blue,
          title: Text(widget.description),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                height: 60,
                width: double.infinity,
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: <Widget>[
                      Expanded(flex: 1, child: Text("Filter by")),
                      Expanded(
                        flex: 3,
                        child: Container(
                          height: 30,
                          width: 190,
                          // width: double.maxFinite,
                          decoration: BoxDecoration(
                            // gradient: getCustomgradient(),
                            color: Colors.white,
                            borderRadius: BorderRadius.all(Radius.circular(6)),
                          ),
                          child: FlatButton(
                            onPressed: () {
                              // Navigator.of(context).pushNamed(HistoryRoute);
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height * 0.1,
                              color: Colors.transparent,
                              child: DropdownButton(
                                underline: Text(""),
                                elevation: 0,
                                autofocus: false,
                                isExpanded: true,
                                iconSize: 40,
                                itemHeight:
                                    MediaQuery.of(context).size.height * 0.3,
                                hint: Center(child: Text("All")),
                                items: listDrop,
                                value: value,
                                onChanged: (res) async {
                                  setState(() {
                                    value = res;
                                  });
                                },
                              ),
                            ),
                            // textColor: Colors.red,
                            shape: RoundedRectangleBorder(
                                side: BorderSide(
                                    color: Colors.blue,
                                    width: 1,
                                    style: BorderStyle.solid),
                                borderRadius: BorderRadius.circular(6)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 8,
              child: Container(
                decoration: BoxDecoration(
                  // image: new DecorationImage(

                  //     image: new AssetImage("assets/images/newbk.png"),

                  //     fit: BoxFit.cover),

                  color: Colors.grey[300],
                ),
                child: FutureBuilder(
                    future: data,
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.connectionState == ConnectionState.none ||
                          snapshot.connectionState == ConnectionState.active ||
                          snapshot.connectionState == ConnectionState.waiting) {
                        return Center(child: loading());
                      } else if (snapshot.connectionState ==
                          ConnectionState.done) {
                        if (snapshot.hasError) {
                          return errorretry(() {
                            setState(() {
                              data = transDetails(
                                  widget.itemCode, widget.username);
                            });
                          });
                        } else if (snapshot.data == null) {
                          return Center(
                            child: Text("Null"),
                          );
                        } else if (snapshot.data != null) {
                          List<Info> data = snapshot.data;

                          if (data.length < 1) {
                            return Center(
                              child: Text("No Data Found"),
                            );
                          } else {
                            return ListView.builder(
                              itemBuilder: (context, index) {
                                return Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: GestureDetector(
                                    onTap: () {
                                      trans.setDetails(data[index]);

                                      Navigator.of(context).pushNamed(
                                          FullDetailsRoute,
                                          arguments: FullDetailsArguments(
                                              widget.description, data[index]));
                                    },
                                    child: Card(
                                      elevation: 5,
                                      shape: RoundedRectangleBorder(
                                          side: BorderSide(
                                              color: Colors.transparent,
                                              width: 0,
                                              style: BorderStyle.solid),
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Container(
                                        decoration: BoxDecoration(
                                          // image: new DecorationImage(

                                          //     image: new AssetImage(

                                          //         "assets/images/newbk.png"),

                                          //     fit: BoxFit.cover),

                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10)),

                                          // color: Colors.grey[300],
                                        ),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                            color: Colors.grey[200],
                                          ),
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Column(
                                              children: <Widget>[
                                                Text(
                                                  data[index].name,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .bodyText2,
                                                ),
                                                Divider(),
                                                Fcard(
                                                    "Account Number",
                                                    data[index].acctLink,
                                                    5.0,
                                                    Colors.white54,
                                                    Colors.black),
                                                Fcard(
                                                    "Amount",
                                                    data[index].currency +
                                                        " " +
                                                        data[index].amount,
                                                    5.0,
                                                    Colors.white,
                                                    Colors.blue),
                                                Fcard(
                                                    "Posted By",
                                                    data[index].postedBy,
                                                    5.0,
                                                    Colors.white,
                                                    Colors.black),
                                                Fcard(
                                                    "Branch",
                                                    data[index].branchName,
                                                    5.0,
                                                    Colors.white,
                                                    Colors.black),
                                                Fcard(
                                                    "Reason",
                                                    "No Reason",
                                                    5.0,
                                                    Colors.white,
                                                    Colors.red),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),

                                    /* child: Card(
  
                                elevation: 5,
  
                                color: Colors.white.withOpacity(0.9),
  
                                child: Padding(
  
                                  padding: const EdgeInsets.all(8.0),
  
                                  child: Column(
  
                                    children: <Widget>[
  
                                      Text(
  
                                        data[index].name,
  
                                        style: Theme.of(context).textTheme.body2,
  
                                      ),
  
                                      Divider(),
  
                                      Fcard(
  
                                          "Account Number",
  
                                          data[index].acctLink,
  
                                          5.0,
  
                                          Colors.blue[50],
  
                                          Colors.black),
  
                                      Fcard(
  
                                          "Amount",
  
                                          data[index].currency +
  
                                              " " +
  
                                              data[index].amount,
  
                                          5.0,
  
                                          Colors.white,
  
                                          Colors.blue),
  
                                      Fcard("Posted By", data[index].postedBy,
  
                                          5.0, Colors.blue[50], Colors.black),
  
                                      Fcard("Branch", data[index].branchName, 5.0,
  
                                          Colors.white, Colors.black),
  
                                      Fcard("Reason", "No Reason", 5.0,
  
                                          Colors.blue[50], Colors.red),
  
                                    ],
  
                                  ),
  
                                ),
  
                              ),*/
                                  ),
                                );
                              },
                              itemCount: data.length,
                            );
                          }

                          /* TransactionModel res = snapshot.data;
  
                      if (res.mbResponse == "00") {
  
                        if (res.mbTransactions.length < 1) {
  
                          return Center(
  
                            child: Text("No data Found"),
  
                          );
  
                        } else {
  
                          return Center(
  
                            child: Text("I have data"),
  
                          );
  
                        }
  
                      } else {
  
                        return Center(
  
                          child: res.mbMessage == null
  
                              ? Text("Couldnt get Data")
  
                              : Text(res.mbMessage),
  
                        );
  
                      }*/

                        } else {
                          return Center(
                            child: Text("error from no where"),
                          );
                        }
                      } else {
                        return Center(
                          child: Text("without snapshot error"),
                        );
                      }
                    }),
              ),
            ),
          ],
        ));
  }
}
