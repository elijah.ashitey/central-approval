import 'package:approval/resources/provider.dart';
import 'package:approval/routing_constants.dart';
import 'package:approval/routinig_objects.dart';
import 'package:approval/widgets/gradient.dart';
import 'package:approval/widgets/homeList.dart';
import 'package:flutter/material.dart';
import 'package:polygon_clipper/polygon_border.dart';
import 'package:polygon_clipper/polygon_clipper.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
    final res = Provider.of<User>(context, listen: false);

    res.refresh();
  }

  @override
  Widget build(BuildContext context) {
    final res = Provider.of<User>(context);

    return Scaffold(
      body: SafeArea(
        child: Consumer<User>(builder: (context, _userData, child) {
          return Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(
                  // color: Colors.blue[50],
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      // mainAxisSize: MainAxisSize.max,

                      children: <Widget>[
                        Expanded(
                          child: Text(
                            "Hello,",
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 15,
                              fontWeight: FontWeight.w900,
                              fontFamily: 'gotham',
                            ),
                          ),
                        ),
                        // SizedBox(
                        //   height: MediaQuery.of(context).size.height * 0.01,
                        // ),
                        Expanded(
                          child: Text(
                            "Elijah Ashitey",
                            style: TextStyle(
                              color: Color(0XFF111E6C),
                              fontSize: 20,
                              fontFamily: 'gotham',
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  // color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(
                          Radius.circular(10),
                          // bottomRight: Radius.circular(30),
                          // bottomLeft: Radius.circular(30),
                        ),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.blue[50],
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: Text(
                                    _userData.getuser.brDesc,
                                    style: TextStyle(color: Color(0XFF111E6C)),
                                  ),
                                ),
                                Divider(),
                                Expanded(
                                  child: approvStatus("Pending", "500",
                                      icon: Icons.timer),
                                ),
                                Expanded(
                                  child: approvStatus("Approved", "1000",
                                      icon: Icons.check_circle),
                                ),
                                Expanded(
                                  child: approvStatus("Rejected", "45",
                                      icon: Icons.cancel),
                                )
                                // Spacer(),

                                // Spacer(),

                                // Spacer(),

                                // ListTile(
                                //   leading: Icon(Icons.ac_unit),
                                //   title: Text("Pending"),
                                //   trailing: Text("5"),
                                // ),
                                // ListTile(
                                //   leading: Icon(Icons.ac_unit),
                                //   title: Text("Pending"),
                                //   trailing: Text("5"),
                                // ),
                                // ListTile(
                                //   leading: Icon(Icons.ac_unit),
                                //   title: Text("Pending"),
                                //   trailing: Text("5"),
                                // ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 7,
                child: Container(
                  color: Colors.grey[100],
                  // color: Color(0xffFEE7B7),
                  child: RefreshIndicator(
                    onRefresh: res.refresh,
                    child: ListView.builder(
                      itemBuilder: (context, index) {
                        return HomeList(_userData.getList[index].numApprovals,
                            _userData.getList[index].description, () {
                          Navigator.pushNamed(context, SumDetailsRoute,
                              arguments: SumDetailsArguments(
                                _userData.getList[index].description,
                                _userData.getList[index].code,
                                _userData.getuser.globalUsername,
                              ));
                          // Navigator.pushNamed(
                          //   context,
                          //   CategoryRoute,
                          //   arguments: _userData
                          //       .getuser.userAccessList[index].description,
                          // );
                        });

                        //  polygon1(
                        //     context,
                        //     _userData.getuser.userAccessList[index].description,
                        //     _userData.getuser.userAccessList[index].numApprovals);
                        // newpolygon(context);
                        /* Container(
                          child: Padding(
                            padding: const EdgeInsets.only(
                              top: 10.0,
                            ),
                            child:
                             polygon1(
                                context,
                                _userData
                                    .getuser.userAccessList[index].description,
                                _userData
                                    .getuser.userAccessList[index].numApprovals),
                          ),
                        );*/
                        // newlist(
                        //     context,
                        //     index,
                        //     _userData.getuser.userAccessList[index].description,
                        //     _userData.getuser.userAccessList[index].numApprovals);
                      },
                      itemCount: _userData.getuser.userAccessList.length,
                    ),
                  ),
                ),
              )
            ],
          );
        }),
      ),
    );
  }
}

list(BuildContext context, int index, description, number) {
  return GestureDetector(
    onTap: () {
      Navigator.pushNamed(context, '/sumDetail');
    },
    child: Card(
      elevation: 1,
      color: Colors.blue[50],
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.2,
              // height: MediaQuery.of(context).size.height * 0.08,
              decoration: BoxDecoration(
                // color: Colors.white,
                color: Colors.blue[50],
              ),
              child: ListTile(
                title: Text(
                  // _userData.getuser.userAccessList[index]
                  //     .description,
                  description,
                  style: TextStyle(fontFamily: 'gotham'),
                ),
                // leading: CircleAvatar(
                //   child: Text("AS"),
                // ),
              ),
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              // bottomRight: Radius.circular(30),
              bottomLeft: Radius.circular(30),
            ),
            child: Container(
              width: MediaQuery.of(context).size.width * 0.15,
              height: MediaQuery.of(context).size.height * 0.08,
              decoration: BoxDecoration(
                gradient: inverse(),
                // boxShadow: [
                //   BoxShadow(
                //     color: Colors.grey,
                //     blurRadius: 15.0,
                //   ),
                // ],
              ),
              child: Center(
                  child: Text(
                // _userData.getuser.userAccessList[index]
                //     .numApprovals,
                number,
                style: TextStyle(color: Colors.white),
              )),
            ),
          ),
        ],
      ),
    ),
  );
}

newlist(BuildContext context, int index, description, number) {
  return GestureDetector(
    onTap: () {
      Navigator.pushNamed(context, '/sumDetail');
    },
    child: Card(
      elevation: 1,
      color: Colors.blue[50],
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.2,
              // height: MediaQuery.of(context).size.height * 0.08,
              decoration: BoxDecoration(
                // color: Colors.white,

                color: Colors.blue[50],
              ),
              child: ListTile(
                title: Text(
                  // _userData.getuser.userAccessList[index]
                  //     .description,
                  description,
                  style: TextStyle(fontFamily: 'gotham'),
                ),
                // leading: CircleAvatar(
                //   child: Text("AS"),
                // ),
              ),
            ),
          ),
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(30),
              // bottomRight: Radius.circular(30),
              bottomLeft: Radius.circular(30),
            ),
            child: Container(
              width: MediaQuery.of(context).size.width * 0.15,
              height: MediaQuery.of(context).size.height * 0.08,
              decoration: BoxDecoration(
                gradient: inverse(),
                // boxShadow: [
                //   BoxShadow(
                //     color: Colors.grey,
                //     blurRadius: 15.0,
                //   ),
                // ],
              ),
              child: Center(
                  child: Text(
                // _userData.getuser.userAccessList[index]
                //     .numApprovals,
                number,
                style: TextStyle(color: Colors.white),
              )),
            ),
          ),
        ],
      ),
    ),
  );
}

polygon(BuildContext context, description, number) {
  return Row(
    children: <Widget>[
      Expanded(
          flex: 2,
          child: ClipPolygon(
            child: Container(
                // width: MediaQuery.of(context).size.width * 0.1,
                // height: MediaQuery.of(context).size.height * 0.1,
                decoration: BoxDecoration(
                  color: Colors.white,
                  // gradient: LinearGradient(
                  //   begin: Alignment.topLeft,
                  //   end: Alignment.bottomRight,
                  //   colors: [
                  //     Colors.grey[200],
                  //     Colors.grey[800],
                  //   ],
                  // ),
                ),
                child: Center(
                  child: Text(
                    number,
                    style: TextStyle(fontSize: 12),
                  ),
                )),
            boxShadows: [
              PolygonBoxShadow(color: Colors.black, elevation: 5.0),
            ],
            sides: 6,
            borderRadius: 1.0,
          )),
      Expanded(
        flex: 10,
        child: Text(
          description,
          style: TextStyle(fontSize: 15, color: Colors.black45),
        ),
      )
    ],
  );
}

newpolygon(BuildContext context) {
  return Row(
    children: <Widget>[
      Expanded(
        flex: 1,
        child: Container(
          height: MediaQuery.of(context).size.height * 0.1,
          width: MediaQuery.of(context).size.width * 0.2,
          padding: EdgeInsets.all(18.0),
          margin: EdgeInsets.only(right: 16.0),
          decoration: ShapeDecoration(
              color: Colors.white,
              shape: PolygonBorder(
                  sides: 7,

                  // boxShadows: [
                  //   PolygonBoxShadow(color: Colors.black, elevation: 5.0),
                  // ],
                  borderRadius: 5.0,
                  border: BorderSide(color: Colors.white, width: 1))),
          child: Center(child: Text('8000')),
        ),
      ),
      Expanded(
        flex: 7,
        child: Container(
          // color: Colors.red,
          child: Text("long text"),
        ),
      )
    ],
  );
}

Widget polygon1(
  BuildContext context,
  description,
  number,
) {
  return Column(
    children: <Widget>[
      ListTile(
        onTap: () {
          print("object");
        },
        leading: ClipPolygon(
          child: Container(
              // width: MediaQuery.of(context).size.width * 0.1,
              // height: MediaQuery.of(context).size.height * 0.1,
              decoration: BoxDecoration(
                color: Colors.white,
                // gradient: LinearGradient(
                //   begin: Alignment.topLeft,
                //   end: Alignment.bottomRight,
                //   colors: [
                //     Colors.grey[200],
                //     Colors.grey[800],
                //   ],
                // ),
              ),
              child: Center(
                child: Text(
                  number,
                  style: TextStyle(
                    fontSize: 12,
                    color: Color(0XFF111E6C),
                  ),
                ),
              )),
          boxShadows: [
            PolygonBoxShadow(color: Colors.black, elevation: 5.0),
          ],
          sides: 6,
          borderRadius: 1.0,
        ),
        title: Text(
          description,
          style: TextStyle(fontSize: 15, color: Colors.black45),
        ),
      ),
      Divider()
    ],
  );
}

approvStatus(text, number, {IconData icon}) {
  // if (icon==1) {

  // } else {
  // }
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      Row(
        children: <Widget>[
          Icon(
            icon,
            size: 12,
            color: Colors.blue,
            // color: Color(0XFF111E6C),
          ),
          SizedBox(width: 10),
          Text(
            text,
            style: TextStyle(fontSize: 13, color: Color(0XFF111E6C)),
          ),
        ],
      ),
      Text(
        number,
        style: TextStyle(fontSize: 14, color: Color(0XFF111E6C)),
      ),
    ],
  );
}
