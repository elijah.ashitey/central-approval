import 'package:approval/widgets/charts/barChart.dart';
import 'package:approval/widgets/charts/model.dart';
import 'package:approval/widgets/charts/pieChart.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:approval/screens/hooooooooome.dart';

import '../routing_constants.dart';

class Summary extends StatefulWidget {
  @override
  _SummaryState createState() => _SummaryState();
}

class _SummaryState extends State<Summary> {
  static List<charts.Series<OrdinalSales, String>> _createSampleBarData() {
    final data = [
      new OrdinalSales('2014', 5),
      new OrdinalSales('2015', 25),
      new OrdinalSales('2016', 100),
      new OrdinalSales('2017', 75),
    ];

    return [
      new charts.Series<OrdinalSales, String>(
        id: 'Sales',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (OrdinalSales sales, _) => sales.year,
        measureFn: (OrdinalSales sales, _) => sales.sales,
        data: data,
      )
    ];
  }

  static List<charts.Series<LinearSales2, int>> _createSampleData() {
    final data = [
      LinearSales2("Seyram", 50, Colors.blue[300], 5),
      LinearSales2("Elijah", 100, Colors.blue[400], 4),
      LinearSales2("Ashitey", 75, Colors.blue[100], 3),
      LinearSales2("Lord", 25, Colors.blue[800], 2),
      LinearSales2("Ruby", 50, Colors.blue[500], 1),
    ];

    return [
      charts.Series<LinearSales2, int>(
        id: 'Trial',
        domainFn: (LinearSales2 sales, _) => sales.dont,
        measureFn: (LinearSales2 sales, _) => sales.sales,
        colorFn: (LinearSales2 sales, _) =>
            charts.ColorUtil.fromDartColor(sales.color),
        data: data,
        labelAccessorFn: (LinearSales2 sales, _) => sales.year,
      )
    ];
  }

  bool _barchart = true;
  bool _piechart = false;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.blue[50],
        body: Column(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.blue[200],
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    color: Colors.white70,
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: Text(
                              "Today",
                              style: TextStyle(color: Color(0XFF111E6C)),
                            ),
                          ),
                          Divider(),
                          Expanded(
                            child: approvStatus("Pending", "500",
                                icon: Icons.timer),
                          ),
                          Expanded(
                            child: approvStatus("Approved", "1000",
                                icon: Icons.check_circle),
                          ),
                          Expanded(
                            child: approvStatus("Rejected", "45",
                                icon: Icons.cancel),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: 50,
              color: Colors.blue[100],
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  FlatButton(onPressed: () {}, child: Text("Today")),
                  VerticalDivider(),
                  FlatButton(onPressed: () {}, child: Text("This week")),
                  VerticalDivider(),
                  FlatButton(onPressed: () {}, child: Text("This Month")),
                  VerticalDivider(),
                  FlatButton(onPressed: () {}, child: Text("More")),
                ],
              ),
            ),
            Expanded(
              flex: 4,
              child: Container(
                // color: Colors.blue[50],
                child: _piechart
                    ? DonutPieChart(_createSampleData())
                    : SimpleBarChart(_createSampleBarData()),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 45,
              width: 190,
              // width: double.maxFinite,
              decoration: BoxDecoration(
                // gradient: getCustomgradient(),
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(6)),
              ),
              child: FlatButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(HistoryRoute);
                },
                child: Text('View Details',
                    style: TextStyle(color: Colors.blue, fontSize: 18.0)),
                // textColor: Colors.red,
                shape: RoundedRectangleBorder(
                    side: BorderSide(
                        color: Colors.blue, width: 1, style: BorderStyle.solid),
                    borderRadius: BorderRadius.circular(6)),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 50,
              color: Colors.blue[100],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                // scrollDirection: Axis.horizontal,
                children: <Widget>[
                  Expanded(
                    child: Container(
                        color: _barchart ? Colors.blue : Colors.transparent,
                        child: FlatButton(
                            onPressed: () {
                              setState(() {
                                _piechart = false;
                                _barchart = true;
                              });
                            },
                            child: Text("Bar chart"))),
                  ),
                  Expanded(
                    child: Container(
                        color: _piechart ? Colors.blue : Colors.transparent,
                        child: FlatButton(
                            onPressed: () {
                              setState(() {
                                _piechart = true;
                                _barchart = false;
                              });
                            },
                            child: Text("Pie Chart"))),
                  ),
                ],
              ),
            ),

            // Expanded(
            //   child: Container(
            //       // color: Colors.yellow,
            //       ),
            // ),
          ],
        ),
      ),
    );
  }
}
