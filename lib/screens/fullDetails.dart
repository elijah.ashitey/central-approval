import 'package:approval/models/account_info.dart';
// import 'package:approval/models/base64.dart';
import 'package:approval/models/detailsModel.dart';
import 'package:approval/resources/amtFormat.dart';
import 'package:approval/resources/apiCalls.dart';
import 'package:approval/resources/provider.dart';
import 'package:approval/routing_constants.dart';
import 'package:approval/widgets/expandable.dart';
import 'package:approval/widgets/heading.dart';
import 'package:approval/widgets/image.dart';
import 'package:approval/widgets/loader.dart';
import 'package:approval/widgets/retry.dart';
import 'package:approval/widgets/sumDetailList.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FullDetails extends StatefulWidget {
  final String title;
  final Info data;
  FullDetails(this.title, this.data);

  @override
  _FullDetailsState createState() => _FullDetailsState();
}

class _FullDetailsState extends State<FullDetails> {
  Future<AccountInfo> accIinfo;
  // Future<Base64Api> data;
  @override
  void initState() {
    super.initState();
    final res = Provider.of<TransactionDetails>(context, listen: false);
    accIinfo = accInfo(res.getDetail.acctLink);
    // data = readBase64(res.getDetail.batchNo);
  }
  /*void viewImage() {
    showDialog(
        context: context,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.5,
                width: MediaQuery.of(context).size.width,
                child: finalImage(),
              ),
//           Container(
//                         height: MediaQuery.of(context).size.height * 0.3,
// child: Row(children:<Widget>[
//   IconButton()
// ]),
//           )
            ]));
  }*/

  @override
  Widget build(BuildContext context) {
    final res = Provider.of<TransactionDetails>(context);

    return Scaffold(
      appBar: (AppBar(
        title: Text(widget.title),
        elevation: 0,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.info,
              color: Colors.white,
            ),
            iconSize: 30,
            // color: Colors.yellow,
            onPressed: () {
              Navigator.of(context)
                  .pushNamed(MandateRoute, arguments: res.getDetail.acctLink);
            },
          ),
          Padding(
            padding: EdgeInsets.only(left: 10),
          ),
        ],
      )),
      body: FutureBuilder(
          future: accIinfo,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.connectionState == ConnectionState.none ||
                snapshot.connectionState == ConnectionState.active ||
                snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: loading());
            } else if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                return retry(() {
                  setState(() {
                    accIinfo = accInfo(res.getDetail.acctLink);
                  });
                }, "No data Found");
              } else if (snapshot.data == null) {
                return Center(
                  child: Text("Null"),
                );
              } else if (snapshot.data != null) {
                AccountInfo resp = snapshot.data;

                if (resp == null) {
                  return Center(
                    child: Text("No data Found"),
                  );
                } else {
                  return Column(
                    children: <Widget>[
                      Expanded(
                        flex: 10,
                        child: Container(
                          decoration: BoxDecoration(
                            image: new DecorationImage(
                                image:
                                    new AssetImage("assets/images/newbk.png"),
                                fit: BoxFit.cover),
                            color: Colors.white,
                          ),
                          child: ListView(
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Card(
                                    margin: EdgeInsets.all(1),
                                    elevation: 0.5,
                                    child: Container(
                                      // color: Colors.grey[100],
                                      // padding: EdgeInsets.all(15.0),
                                      child: Padding(
                                        padding: const EdgeInsets.all(7.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Heading("Transaction Details"),
                                            Fcard(
                                              "Name",
                                              res.getDetail.name,
                                              7,
                                              Colors.white,
                                              Colors.white,
                                            ),
                                            Divider(),
                                            Fcard(
                                              "Account Number",
                                              res.getDetail.acctLink,
                                              7,
                                              Colors.white,
                                              Colors.white,
                                            ),
                                            Divider(),
                                            Fcard(
                                              "Amount",
                                              res.getDetail.currency +
                                                  " " +
                                                  formatAmounts(
                                                      res.getDetail.amount),
                                              7,
                                              Colors.white,
                                              Colors.white,
                                            ),
                                            Divider(),
                                            Fcard(
                                              "Cheque No",
                                              res.getDetail.documentRef,
                                              7,
                                              Colors.white,
                                              Colors.white,
                                            ),
                                            Divider(),
                                            Fcard(
                                              "Narration",
                                              res.getDetail.narration,
                                              7,
                                              Colors.white,
                                              Colors.white,
                                            ),
                                            Divider(),
                                            Container(
                                                height: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.09,
                                                child: ImgScreen(
                                                    res.getDetail.batchNo)
                                                /*Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    child: Container(
                                                      color: Colors.red,
                                                      child: Column(
                                                        children: <Widget>[
                                                          Expanded(
                                                            child: Container(
                                                              color:
                                                                  Colors.blue,
                                                            ),
                                                          ),
                                                          Text("Front Image")
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                  Expanded(
                                                    child: Container(
                                                      color: Colors.yellow,
                                                    ),
                                                  ),
                                                ],
                                              ),*/
                                                )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  AccInfo(resp),
                                  /* Card(
                                    margin: EdgeInsets.all(1),
                                    elevation: 0.5,
                                    child: Container(
                                      // color: Colors.grey[100],
                                      // padding: EdgeInsets.all(15.0),
                                      child: Padding(
                                        padding: const EdgeInsets.all(7.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Heading("Account Details"),
                                            // Divider(
                                            //   indent: 100,
                                            //   endIndent: 100,
                                            //   height: 5,
                                            // ),
                                            Fcard(
                                              "Account Number",
                                              res.getDetail.acctLink,
                                              7,
                                              Colors.white,
                                              Colors.white,
                                            ),
                                            Divider(),
                                            Fcard(
                                              "Name",
                                              res.getDetail.name,
                                              7,
                                              Colors.white,
                                              Colors.white,
                                            ),
                                            Divider(),
                                            Fcard(
                                              "Branch Name",
                                              res.getDetail.branchName,
                                              7,
                                              Colors.white,
                                              Colors.white,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),*/

                                  SizedBox(
                                    height: 10,
                                  ),
                                  Card(
                                    margin: EdgeInsets.all(1),
                                    elevation: 0.5,
                                    child: Container(
                                      // color: Colors.grey[100],
                                      // padding: EdgeInsets.all(15.0),
                                      child: Padding(
                                        padding: const EdgeInsets.all(7.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Heading("User Details"),
                                            Fcard(
                                              "Posted By",
                                              res.getDetail.postedBy,
                                              7,
                                              Colors.transparent,
                                              Colors.transparent,
                                            ),
                                            Divider(),
                                            Fcard(
                                              "Posting Date",
                                              res.getDetail.postingDate,
                                              7,
                                              Colors.white,
                                              Colors.white,
                                            ),
                                            Divider(),
                                            Fcard(
                                              "Posting time",
                                              res.getDetail.time,
                                              7,
                                              Colors.white,
                                              Colors.white,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Container(
                          color: Colors.blue[50],
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                flex: 2,
                                child: Container(
                                  height: double.infinity,
                                  width: double.infinity,
                                  color: Colors.red[400],
                                  child: FlatButton(
                                    child: Text(
                                      "Reject",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20),
                                    ),
                                    onPressed: () => Navigator.of(context)
                                        .pushNamed(RjectRoute),
                                  ),
                                ),
                              ),
                              Expanded(
                                flex: 3,
                                child: Container(
                                  height: double.infinity,
                                  width: double.infinity,
                                  color: Colors.green[400],
                                  child: FlatButton(
                                    child: Text(
                                      "Accept",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 20),
                                    ),
                                    onPressed: () => Navigator.of(context)
                                        .pushNamed(AcceptRoute),
                                    // onPressed: () => Navigator.push(
                                    //   context,
                                    //   MaterialPageRoute(
                                    //       builder: (context) =>
                                    // ImgScreen(res.getDetail.batchNo)),
                                    // ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  );
                }
              } else {
                return Center(
                  child: Text("error from no where"),
                );
              }
            } else {
              return Center(
                child: Text("without snapshot error"),
              );
            }
          }),
    );
  }
}
