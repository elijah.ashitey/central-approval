import 'package:approval/widgets/sumDetailList.dart';
import 'package:flutter/material.dart';

class TransactionHistory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                color: Colors.white,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.arrow_back),
                        onPressed: () => Navigator.pop(context)),
                    // Center(child: Text("Transaction History")),
                    // Row(
                    //   children: <Widget>[
                    //     IconButton(
                    //         icon: Icon(Icons.arrow_back),
                    //         onPressed: () => Navigator.pop(context)),
                    //     Center(child: Text("Transaction History")),
                    //   ],
                    // ),
                    // Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Text("Rejected"),
                            Text("1000"),
                          ],
                        ),
                        Column(
                          children: <Widget>[
                            Text("Approved"),
                            Text("1000"),
                          ],
                        )
                      ],
                    ),
                    Divider(),
                    Center(child: Text("Transaction History")),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 5,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                  ),
                  color: Colors.blue[50],
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView.builder(
                    itemBuilder: (context, index) {
                      return Card(
                        color: Colors.red,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Container(
                            color: Colors.grey[100],
                            child: Container(
                                child: Column(
                              children: <Widget>[
                                Fcard("Account Number", "dsd", 5.0,
                                    Colors.white54, Colors.black),
                                Fcard("Amount", "asas", 5.0, Colors.white,
                                    Colors.blue),
                                Fcard("Posted By", "sqswq", 5.0, Colors.white,
                                    Colors.black),
                                Fcard("Branch", "Adad", 5.0, Colors.white,
                                    Colors.black),
                                Fcard("Reason", "No Reason", 5.0, Colors.white,
                                    Colors.red),
                              ],
                            )),
                          ),
                        ),
                      );
                    },
                    itemCount: 5,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
