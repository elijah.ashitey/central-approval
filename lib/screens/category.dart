// import 'package:approval/widgets/homeList.dart';
import 'package:flutter/material.dart';

class Categories extends StatefulWidget {
  final String title;
  Categories(this.title);
  @override
  _CategoriesState createState() => _CategoriesState();
}

class _CategoriesState extends State<Categories> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        elevation: 0,
      ),
      body: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 15,
              ),
              Expanded(
                child: ListView.builder(
                  itemBuilder: (context, index) {
                    return listData();
                  },
                  itemCount: 8,
                ),
              ),
            ],
          )),
    );
  }
}

listData() {
  return Column(
    children: <Widget>[
      Card(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
          child: ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.blue[50],
              child: Icon(
                Icons.clear_all,
                color: Colors.blue,
              ),
            ),
            title:
                Text("sddsdsdsdsdkassdsdmfsldsdnsdksjdskdjsdsjdhkabskdjabskjb"),
            trailing: CircleAvatar(
              backgroundColor: Colors.blue,
              child: Text(
                "5000",
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 12,
                ),
              ),
            ),
          ),
        ),
      ),
      // Divider(),
    ],
  );
}
