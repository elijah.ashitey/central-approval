import 'package:approval/resources/provider.dart';
import 'package:approval/widgets/gradient.dart';
import 'package:approval/widgets/homeWidgets.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Consumer<User>(builder: (context, _userData, child) {
        return Column(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                // color: Colors.white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 15.0, top: 10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Hello,",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 15,
                                fontWeight: FontWeight.w900,
                                fontFamily: 'gotham',
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.01,
                            ),
                            Text(
                              "Elijah Ashitey",
                              style: TextStyle(
                                // color: Colors.black,
                                fontSize: 20,
                                fontFamily: 'gotham',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: Card(
                        elevation: 8,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            // mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              // approveInfo("pending", "25", 1),
                              // approveInfo("pending", "25", 2),
                              // approveInfo("pending", "25", 3),
                              approvalStatusCard(context, 1),
                              approvalStatusCard(context, 2),
                              approvalStatusCard(context, 3),
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 7,
              child: Container(
                // color: Colors.grey[300],

                // color: Color(0xffFEE7B7),
                child: ListView.builder(
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, '/sumDetail');
                      },
                      child: Card(
                        elevation: 1,
                        color: Colors.blue[50],
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.2,
                                // height: MediaQuery.of(context).size.height * 0.08,
                                decoration: BoxDecoration(
                                  // color: Colors.white,
                                  color: Colors.blue[50],
                                ),
                                child: ListTile(
                                  title: Text(
                                    _userData.getuser.userAccessList[index]
                                        .description,
                                    style: TextStyle(fontFamily: 'gotham'),
                                  ),
                                  // leading: CircleAvatar(
                                  //   child: Text("AS"),
                                  // ),
                                ),
                              ),
                            ),
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(30),
                                // bottomRight: Radius.circular(30),
                                bottomLeft: Radius.circular(30),
                              ),
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.15,
                                height:
                                    MediaQuery.of(context).size.height * 0.08,
                                decoration: BoxDecoration(
                                  gradient: inverse(),
                                  // boxShadow: [
                                  //   BoxShadow(
                                  //     color: Colors.grey,
                                  //     blurRadius: 15.0,
                                  //   ),
                                  // ],
                                ),
                                child: Center(
                                    child: Text(
                                  _userData.getuser.userAccessList[index]
                                      .numApprovals,
                                  style: TextStyle(color: Colors.white),
                                )),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  itemCount: _userData.getuser.userAccessList.length,
                ),
              ),
            )
          ],
        );
      }),
    );
  }
}
