// import 'package:approval/resources/biometricLogin.dart';
import 'package:approval/resources/biometricLogin.dart';
import 'package:approval/resources/provider.dart';
import 'package:approval/widgets/gradient.dart';
import 'package:approval/widgets/loader.dart';
// import 'package:approval/resources/sharedPreference.dart';
import 'package:approval/widgets/topWidget.dart';
import 'package:flutter/material.dart';
// import 'package:gradient_text/gradient_text.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';

class BiometricLogin extends StatefulWidget {
  // final String name;
  // BiometricLogin(this.name);
  @override
  _BiometricLoginState createState() => _BiometricLoginState();
}

class _BiometricLoginState extends State<BiometricLogin> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController password = new TextEditingController();
  // bool _isLoading = false;
  bool _isHidden = true;
  // bool _bio = false;
  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  // var name;
  // var password;
  // @override
  // void initState() {
  //   super.initState();
  //   name = getusername();
  //   password = getpassword();
  // }

  final LocalAuthentication auth = LocalAuthentication();

  @override
  Widget build(BuildContext context) {
    final savedname = Provider.of<User>(context);

    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.transparent,
            // gradient: more()
            // image: new DecorationImage(
            //     image: new AssetImage("assets/images/newbk.png"),
            //     fit: BoxFit.cover),
          ),
          height: MediaQuery.of(context).size.height * 0.4,
          child: TopBar(),
        ),

        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            decoration: BoxDecoration(

                // image: new DecorationImage(
                //     image: new AssetImage("assets/images/newbk.png"),
                //     fit: BoxFit.cover),
                color: Colors.transparent,
                // color: Theme.of(context).backgroundColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(0),
                  topRight: Radius.circular(0),
                )),
            height: MediaQuery.of(context).size.height * 0.53,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Card(
                      elevation: 5,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 30.0, left: 8, right: 8, bottom: 8),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            // ],),

                            Text(
                              "Welcome,",
                              style: TextStyle(
                                color: Colors.grey,
                                fontSize: 12,
                              ),
                            ),
                            SizedBox(
                              height: 12,
                            ),
                            Text(
                              savedname.getSavedName,
                              style: TextStyle(
                                fontSize: 17,
                                color: Colors.blue,
                              ),
                            ),
                            Divider(),

                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.01,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: TextFormField(
                                obscureText: _isHidden,
                                controller: password,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'please enter password';
                                  }
                                  return null;
                                },
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 18,
                                  letterSpacing: 1,
                                  fontWeight: FontWeight.w500,
                                ),
                                decoration: InputDecoration(
                                  suffixIcon: IconButton(
                                    color: Theme.of(context).primaryColor,
                                    onPressed: _toggleVisibility,
                                    icon: _isHidden
                                        ? Icon(Icons.visibility_off)
                                        : Icon(Icons.visibility),
                                  ),
                                  labelText: "Password",

                                  labelStyle:
                                      Theme.of(context).textTheme.bodyText1,

                                  enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(
                                      color: Theme.of(context).buttonColor,
                                    ),
                                  ),

                                  focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                    width: 2.0,
                                    color: Theme.of(context).primaryColor,
                                  )),
                                  //border: InputBorder.none,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).size.height * 0.04,
                            ),
                            Consumer<Default>(
                                builder: (context, _loading, child) {
                              if (_loading.getIsLoadind) {
                                return Center(
                                  child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: loading()),
                                );
                              } else {
                                return Column(
                                  children: <Widget>[
                                    FlatButton(
                                        onPressed: () async {
                                          if (_formKey.currentState
                                              .validate()) {
                                            rememberMelogin(
                                                context, password.text);
                                            /* // setState(() {
                                      //   _isLoading = true;
                                      // });
                                      print(username.text);
                                      print(password.text);
                                      final check = await internetCheck();
                                      if (check) {
                                        print("i have i internet");
                                      } else {
                                        print("No internet");
                                      }

                                      // Navigator.pushNamed(context, '/home');
                                      // error(context);*/

                                            /* try {
                                          setState(() {
                                            _isLoading = true;
                                          });
                                          final checkinternet =
                                              await internetCheck();
                                          if (checkinternet) {
                                            print(username.text);
                                            print(password.text);
                                            final res = await login(
                                                username.text, password.text);
                                            userData.setuser(res);
                                            print("rember me:" +
                                                rememberMe.toString());
                                            if (rememberMe) {
                                              save("username", username.text);
                                              save("password", password.text);
                                              print("user info saved");
                                            } else {
                                              print("Noooooooooooo data saved");
                                            }
                                            Navigator.pushNamed(
                                                context, HomePageRoute);
                                          } else {
                                            info(context, "No Internet Access");
                                          }
                                        } on PlatformException catch (e) {
                                          print("Error:${e.message}");
                                          err(context, e.message.toString());
                                        } finally {
                                          setState(() {
                                            _isLoading = false;
                                          });
                                        }*/
                                          }
                                        },
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text(
                                              "Sign In",
                                              style: TextStyle(
                                                  // fontFamily: 'gotham',
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 22),
                                            ),
                                            Container(
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                gradient: basic(),
                                              ),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Icon(
                                                  Icons.arrow_forward,
                                                  size: 25,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                            // CircleAvatar(
                                            //   child: Icon(Icons.arrow_forward),
                                            // )
                                          ],
                                        )),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    savedname.getBio
                                        ? Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Container(
                                              color: Colors.blue,
                                              child: FlatButton(
                                                  onPressed: () =>
                                                      biologin(context),
                                                  child: Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Text(
                                                        "Login with biometric",
                                                        style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 15,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width: 10,
                                                      ),
                                                      Icon(
                                                        Icons.fingerprint,
                                                        size: 30,
                                                        color: Colors.white,
                                                      ),
                                                    ],
                                                  )),
                                            )

                                            //  IconButton(
                                            //     icon: Icon(
                                            //       Icons.fingerprint,
                                            //       size: 50,
                                            //     ),
                                            //     onPressed: () {}),
                                            )
                                        : Text(""),
                                  ],
                                );
                              }
                            }),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        )
        // Align(
        //   alignment: Alignment.bottomCenter,
        //   child: Container(
        //     color: Colors.white,
        //     height: MediaQuery.of(context).size.height * 0.6,
        //     child: Center(
        //       child: Column(
        //         children: <Widget>[
        //           Text(
        //             " Welcome, ",
        //             style: TextStyle(color: Colors.black),
        //           ),
        //           Text(
        //             savedname.getSavedName,
        //             style: TextStyle(color: Colors.black),
        //           )
        //         ],
        //       ),
        //     ),
        //   ),
        // ),

        // Center(
        //     child: FlatButton(
        //         onPressed: () async {
        //           bool ishasBiometric = await auth.canCheckBiometrics;
        //           if (ishasBiometric) {
        //             final res = await authenticate();
        //             if (res) {
        //               print("bio correct");
        //             } else {
        //               print("failed bio");
        //             }
        //           } else {
        //             print("No bio found");
        //           }
        //         },
        //         child: Text("finger Print")))
      ],
    ));
  }
}
