import 'package:approval/resources/apiCalls.dart';
import 'package:approval/resources/connectionChecker.dart';
import 'package:approval/resources/provider.dart';
import 'package:approval/widgets/card.dart';
import 'package:approval/widgets/dialogs.dart';
import 'package:approval/widgets/loader.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../routing_constants.dart';

class RejectScreen extends StatefulWidget {
  @override
  _RejectScreenState createState() => _RejectScreenState();
}

class _RejectScreenState extends State<RejectScreen> {
  @override
  void initState() {
    super.initState();
    final rejectreason =
        Provider.of<RejectReasonProvider>(context, listen: false);
    rejectreason.setCode(null);
    rejectreason.setComment(null);
  }

  String comt;
  List<DropdownMenuItem<String>> listDrop = [];

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    final res = Provider.of<TransactionDetails>(context);
    final userDet = Provider.of<User>(context);
    final rejectreasonList = Provider.of<RejectReasonList>(context);
    final rejectresaon = Provider.of<RejectReasonProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("CONFIRM REJECTION"),
        elevation: 0,
        // backgroundColor: Colors.lightBlueAccent,
        actions: <Widget>[
          IconButton(
            onPressed: () => Navigator.pop(context),
            icon: Icon(
              Icons.close,
              color: Colors.white,
            ),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 8,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView(
                children: <Widget>[
                  Fcard(
                    "Account Number",
                    res.getDetail.acctLink,
                    20,
                    Colors.transparent,
                  ),
                  Fcard(
                    "Account Name",
                    res.getDetail.name,
                    20,
                    Colors.grey[200],
                  ),
                  Fcard(
                    "Account Branch ",
                    res.getDetail.branchName,
                    20,
                    Colors.transparent,
                  ),
                  Fcard(
                    "Amount",
                    res.getDetail.currency + " " + res.getDetail.amount,
                    20,
                    Colors.grey[200],
                  ),
                  Fcard(
                    "Cheque No",
                    res.getDetail.documentRef,
                    20,
                    Colors.transparent,
                  ),
                  Fcard(
                    "Narration",
                    res.getDetail.narration,
                    20,
                    Colors.grey[200],
                  ),
                  Fcard(
                    "Posted By",
                    res.getDetail.postedBy,
                    20,
                    Colors.transparent,
                  ),
                  Fcard(
                    "Posting Date",
                    res.getDetail.postingDate,
                    20,
                    Colors.grey[200],
                  ),
                  Fcard(
                    "Posting time",
                    res.getDetail.time,
                    20,
                    Colors.transparent,
                  ),
                  Consumer<RejectReasonProvider>(
                      builder: (context, _reason, child) {
                    if (_reason.getComment != null) {
                      return Fcard2(
                          "Comment", _reason.getComment, 20, Colors.grey[200],
                          () {
                        Navigator.of(context).pushNamed(RjectWidgetRoute);
                      });
                    } else {
                      return Container();
                    }
                  }),
                  /*Row(
                          children: <Widget>[
                            Expanded(flex: 2, child: Text("comment:")),
                            Expanded(
                              flex: 4,
                              child: GestureDetector(
                                onTap: () {
                                  rejectreason.setReasons(rejectreasonData);

                                  Navigator.of(context)
                                      .pushNamed(RjectWidgetRoute);
                                },
                                child: Consumer<RejectReasonProvider>(
                                    builder: (context, _reason, child) {
                                  return Card(
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Center(
                                        child: Text(_reason.getComment ??
                                            "select reject reason"),
                                      ),
                                    ),
                                  );
                                }),
                              ),
                            ),
                          ],
                        ),*/
                ],
              ),
            ),
          ),
          _isLoading
              ? Center(child: loading())
              : Container(
                  height: 45,
                  width: MediaQuery.of(context).size.width / 1.2,
                  decoration: BoxDecoration(
                    // gradient: getCustomgradient(),
                    color: Colors.lightBlueAccent,
                    borderRadius: BorderRadius.all(Radius.circular(6)),
                  ),
                  child: FlatButton(
                    onPressed: () async {
                      if (rejectresaon.getComment == null) {
                        rejectreasonList.setReasons(rejectreasonData);

                        Navigator.of(context).pushNamed(RjectWidgetRoute);
                      } else {
                        try {
                          setState(() {
                            _isLoading = true;
                          });
                          final checkinternet = await internetCheck();
                          if (checkinternet) {
                            final username = userDet.getuser.globalUsername;
                            print("Comment: " + rejectresaon.getComment);
                            final resp = await approvAPI(
                                username,
                                res.getDetail.otherInfo,
                                'N',
                                rejectresaon.getComment,
                                res.getDetail.batchNo);
                            print(username);
                            print(res.getDetail.otherInfo);
                            print(res.getDetail.batchNo);
                            if (resp.response != null) {
                              err(
                                context,
                                resp.response,
                              );
                            } else {
                              success(
                                  context,
                                  res.getDetail.batchNo +
                                      " has been rejected successfully", () {
                                Navigator.of(context).pushNamed(HomePageRoute);
                              });
                            }
                          } else {
                            info(context, "No Internet Access");
                          }
                        } on PlatformException catch (e) {
                          print("Error:${e.message}");
                          err(context, e.message.toString());
                        } finally {
                          setState(() {
                            _isLoading = false;
                          });
                        }
                      }
                    },

                    child: Text('REJECT',
                        style: TextStyle(color: Colors.white, fontSize: 22.0)),
                    // textColor: Colors.red,
                    shape: RoundedRectangleBorder(
                        side: BorderSide(
                            color: Colors.white,
                            width: 1,
                            style: BorderStyle.solid),
                        borderRadius: BorderRadius.circular(6)),
                  ),
                ),
          SizedBox(
            height: 10,
          )
        ],
      ),
    );
  }
/*
  void _onButtonPressed() {
    final rejectreason = Provider.of<RejectReasonList>(context, listen: false);
    rejectreason.setReasons(rejectreasonData);
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            // color: Color(0xFF737373),
            color: Colors.white,
            // height: MediaQuery.of(context).size.height * 0.23,
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
              )),
              child: _details(),
            ),
          );
        });
  }

  Widget _details() {
    final rejectreasonlist =
        Provider.of<RejectReasonList>(context, listen: false);
    final rejectreason =
        Provider.of<RejectReasonProvider>(context, listen: false);

    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        if (rejectreason.getCode == rejectreasonlist.getReasons[index].code) {
          return ListTile(
            onTap: () {
              rejectreason
                  .setComment(rejectreasonlist.getReasons[index].reject);
              rejectreason.setCode(rejectreasonlist.getReasons[index].code);

              Navigator.pop(context);
            },
            title: Text(rejectreasonlist.getReasons[index].reject),
            trailing: Icon(Icons.check),
          );
        } else {
          return ListTile(
            onTap: () {
              rejectreason
                  .setComment(rejectreasonlist.getReasons[index].reject);
              rejectreason.setCode(rejectreasonlist.getReasons[index].code);

              Navigator.pop(context);
            },
            title: Text(rejectreasonlist.getReasons[index].reject),
          );
        }
      },
      itemCount: 5,
    );
  }*/
}

class RejectReason {
  final String reject;
  final int code;
  RejectReason(this.reject, this.code);
}

final RejectReason one = RejectReason("Elijah Ashitey is my name", 1);
final RejectReason two = RejectReason(
    "i rejected because the funds are too much. it has exceeded the limit", 2);
final RejectReason three = RejectReason("reason 3", 3);
final RejectReason four = RejectReason("reason 4", 4);
final RejectReason last = RejectReason("other", 00);

List<RejectReason> rejectreasonData = [one, two, three, four, last];
