// import 'package:approval/screens/home.dart';
import 'package:approval/screens/settings.dart';
import 'package:approval/screens/summary.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';

import 'package:flutter/material.dart';

import 'hooooooooome.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int currentIndex = 0;
  Widget _pageChoser(int page) {
    switch (page) {
      case 0:
        return Home();
        break;
      case 1:
        return Summary();
        break;
      case 2:
        return Settings();

      //   break;
      // case 3:
      //   return SettingsScreen();
      //   break;
      default:
        return Container(
            child: Center(
          child: Text("No page found"),
        ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _pageChoser(currentIndex),
      bottomNavigationBar: BottomNavyBar(
        backgroundColor: Colors.white,
        selectedIndex: currentIndex,
        showElevation: true,
        onItemSelected: (index) => setState(() {
          currentIndex = index;
        }),
        items: [
          BottomNavyBarItem(
            icon: Icon(Icons.home),
            title: Text(
              'Home',
              style: TextStyle(fontSize: 12),
            ),
            inactiveColor: Colors.grey,
            activeColor: Colors.blue,
          ),
          BottomNavyBarItem(
            icon: Icon(Icons.apps),
            title: Text(
              'Summary',
              style: TextStyle(fontSize: 12),
            ),
            inactiveColor: Colors.grey,
            activeColor: Colors.blue,
          ),
          /*
          BottomNavyBarItem(
            icon: Icon(Icons.message),
            title: Text('Messages'),
            inactiveColor: Colors.grey,
            activeColor: Colors.blue,
          ),*/
          BottomNavyBarItem(
            icon: Icon(Icons.settings),
            title: Text(
              'Settings',
              style: TextStyle(fontSize: 12),
            ),
            inactiveColor: Colors.grey,
            activeColor: Colors.blue,
          ),
        ],
      ),
    );
  }
}
