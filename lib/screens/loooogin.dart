import 'package:approval/resources/apiCalls.dart';
import 'package:approval/resources/connectionChecker.dart';
import 'package:approval/resources/provider.dart';
import 'package:approval/resources/sharedPreference.dart';
import 'package:approval/routing_constants.dart';
import 'package:approval/widgets/dialogs.dart';
// import 'package:approval/widgets/dialogs.dart';
import 'package:approval/widgets/gradient.dart';
import 'package:approval/widgets/loader.dart';
import 'package:approval/widgets/topWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:gradient_text/gradient_text.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController username = new TextEditingController();
  TextEditingController password = new TextEditingController();
  bool _isLoading = false;
  bool _isHidden = true;
  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }

  bool rememberMe = false;

  /*void _onRememberMeChanged(bool newValue) => setState(() {
        rememberMe = newValue;

        if (rememberMe) {
          print("remember me");
        } else {
          print("No remember me");
        }
      });*/

  @override
  Widget build(BuildContext context) {
    // final themeData = Provider.of<ThemeState>(context);
    final userData = Provider.of<User>(context);

    return Scaffold(
      backgroundColor: Colors.blue[50],
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              color: Colors.transparent,
              // gradient: more()
              // image: new DecorationImage(
              //     image: new AssetImage("assets/images/newbk.png"),
              //     fit: BoxFit.cover),
            ),
            height: MediaQuery.of(context).size.height * 0.4,
            child: TopBar(),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(

                  // image: new DecorationImage(
                  //     image: new AssetImage("assets/images/newbk.png"),
                  //     fit: BoxFit.cover),
                  color: Colors.transparent,
                  // color: Theme.of(context).backgroundColor,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(0),
                    topRight: Radius.circular(0),
                  )),
              height: MediaQuery.of(context).size.height * 0.53,
              width: MediaQuery.of(context).size.width,
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Card(
                        elevation: 5,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: <Widget>[
                              // ],),

                              Text("Login"),
                              Divider(),

                              Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: TextFormField(
                                  controller: username,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'please enter username';
                                    }
                                    return null;
                                  },
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18,
                                    letterSpacing: 1,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  decoration: InputDecoration(
                                    labelText: "Username",
                                    labelStyle:
                                        Theme.of(context).textTheme.bodyText1,
                                    // TextStyle(
                                    //   fontWeight: FontWeight.bold,
                                    //   color: Colors.blue,
                                    //   fontFamily: 'gotham',
                                    //   letterSpacing: 1,
                                    // ),

                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Theme.of(context).buttonColor,
                                      ),
                                    ),

                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                      width: 2.0,
                                      color: Theme.of(context).primaryColor,
                                    )),
                                    //border: InputBorder.none,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.01,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: TextFormField(
                                  obscureText: _isHidden,
                                  controller: password,
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'please enter password';
                                    }
                                    return null;
                                  },
                                  style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 18,
                                    letterSpacing: 1,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  decoration: InputDecoration(
                                    suffixIcon: IconButton(
                                      color: Theme.of(context).primaryColor,
                                      onPressed: _toggleVisibility,
                                      icon: _isHidden
                                          ? Icon(Icons.visibility_off)
                                          : Icon(Icons.visibility),
                                    ),
                                    labelText: "Password",

                                    labelStyle:
                                        Theme.of(context).textTheme.bodyText1,

                                    enabledBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(
                                        color: Theme.of(context).buttonColor,
                                      ),
                                    ),

                                    focusedBorder: UnderlineInputBorder(
                                        borderSide: BorderSide(
                                      width: 2.0,
                                      color: Theme.of(context).primaryColor,
                                    )),
                                    //border: InputBorder.none,
                                  ),
                                ),
                              ),
                              SizedBox(
                                height:
                                    MediaQuery.of(context).size.height * 0.04,
                              ),
                              _isLoading
                                  ? Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: loading()
                                      // LoadingBouncingGrid.square(
                                      //   size: 30,
                                      //   backgroundColor: Colors.blue,
                                      // ),
                                      )
                                  : FlatButton(
                                      onPressed: () async {
                                        if (_formKey.currentState.validate()) {
                                          /* // setState(() {
                                      //   _isLoading = true;
                                      // });
                                      print(username.text);
                                      print(password.text);
                                      final check = await internetCheck();
                                      if (check) {
                                        print("i have i internet");
                                      } else {
                                        print("No internet");
                                      }

                                      // Navigator.pushNamed(context, '/home');
                                      // error(context);*/

                                          try {
                                            setState(() {
                                              _isLoading = true;
                                            });
                                            final checkinternet =
                                                await internetCheck();
                                            if (checkinternet) {
                                              print(username.text);
                                              print(password.text);
                                              final res = await login(
                                                  username.text, password.text);
                                              userData.setuser(res);
                                              print("rember me:" +
                                                  rememberMe.toString());
                                              if (rememberMe) {
                                                save("username", username.text);
                                                save("password", password.text);
                                                print("user info saved");
                                              } else {
                                                print(
                                                    "Noooooooooooo data saved");
                                              }
                                              Navigator.pushNamed(
                                                  context, HomePageRoute);
                                            } else {
                                              info(context,
                                                  "No Internet Access");
                                            }
                                          } on PlatformException catch (e) {
                                            print("Error:${e.message}");
                                            err(context, e.message.toString());
                                          } finally {
                                            setState(() {
                                              _isLoading = false;
                                            });
                                          }
                                        }
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            "Sign In",
                                            style: TextStyle(
                                                // fontFamily: 'gotham',
                                                fontWeight: FontWeight.bold,
                                                fontSize: 22),
                                          ),
                                          Container(
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              gradient: basic(),
                                            ),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Icon(
                                                Icons.arrow_forward,
                                                size: 25,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                          // CircleAvatar(
                                          //   child: Icon(Icons.arrow_forward),
                                          // )
                                        ],
                                      )),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Checkbox(
                                      value: rememberMe,
                                      onChanged: (val) {
                                        setState(() {
                                          rememberMe = val;
                                          print(
                                              "rember me from the remember me widget:" +
                                                  rememberMe.toString());
                                          // if(rememberMe){
                                          //   // saveDataN( username.text);
                                          // }
                                        });
                                      }),
                                  Text("Remember me"),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      Spacer(),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FlatButton(
                    onPressed: () {},
                    child: Text(
                      "Sign up",
                      style: TextStyle(
                          fontFamily: 'gotham',
                          fontWeight: FontWeight.bold,
                          fontSize: 18),
                    )),
                FlatButton(
                  onPressed: () {
                    // themeData.setTheme();
                  },
                  child: GradientText("Forgot Password ?",
                      gradient: basic(),
                      // gradient: LinearGradient(colors: [
                      //   Colors.blue,
                      //   Colors.b,
                      //   Colors.pink
                      // ]),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                          fontFamily: 'gotham'),
                      textAlign: TextAlign.center),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
