import 'dart:convert';
import 'dart:typed_data';

import 'package:approval/models/mandateModel.dart';
import 'package:approval/resources/apiCalls.dart';
import 'package:approval/widgets/loader.dart';
import 'package:approval/widgets/retry.dart';
import 'package:flutter/material.dart';

class MandateScreen extends StatefulWidget {
  final String accountNumber;
  MandateScreen(
    this.accountNumber,
  );
  @override
  _MandateScreenState createState() => _MandateScreenState();
}

class _MandateScreenState extends State<MandateScreen> {
  Future<MandateModel> data;
  @override
  void initState() {
    super.initState();
    data = fetchMandate(widget.accountNumber);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Mandate')),
        backgroundColor: Colors.white,
        body: FutureBuilder(
            future: data,
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.none ||
                  snapshot.connectionState == ConnectionState.active ||
                  snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: loading());
              } else if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  print(snapshot.error);
                  return retry(() {
                    setState(() {
                      data = fetchMandate(widget.accountNumber);
                    });
                  }, "No data Found");
                } else if (snapshot.data == null) {
                  return Center(
                    child: Text("Null"),
                  );
                } else if (snapshot.data != null) {
                  MandateModel res = snapshot.data;
                  if (res.responseCode == "00") {
                    if (res.data.relations.length < 1) {
                      return Center(
                        child: Text("No data Found"),
                      );
                    } else {
                      String _sign = res.data.relations[0].signature
                          .replaceAll(new RegExp(r"\s+"), "");
                      String _photo = res.data.relations[0].photograph
                          .replaceAll(new RegExp(r"\s+"), "");
                      Uint8List signature = base64.decode(_sign);
                      Uint8List picture = base64.decode(_photo);
                      return ListView(children: <Widget>[
                        Container(
                          // color: Colors.black,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              SizedBox(
                                height: 30,
                              ),
                              Card(
                                elevation: 5,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    child: Image.memory(signature),
                                  ),
                                ),
                              ),
                              Center(
                                child: Text(
                                  "Signature",
                                  style: TextStyle(color: Colors.blue),
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Card(
                                elevation: 5,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    color: Colors.grey,
                                    // width: MediaQuery.of(context).size.width / 3,
                                    // height:
                                    //     MediaQuery.of(context).size.height / 2.6,
                                    child: Image.memory(picture),
                                  ),
                                ),
                              ),
                              Center(
                                child: Text(
                                  "Picture",
                                  style: TextStyle(color: Colors.blue),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ]);
                    }
                  } else {
                    return Center(
                      child: res.message == null
                          ? Text("Couldnt get Data")
                          : Text(res.message),
                    );
                  }
                } else {
                  return Center(
                    child: Text("error from no where"),
                  );
                }
              } else {
                return Center(
                  child: Text("without snapshot error"),
                );
              }
            }));
  }
}
