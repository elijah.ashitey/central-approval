import 'package:approval/models/account_info.dart';
import 'package:approval/resources/apiCalls.dart';
import 'package:approval/routing_constants.dart';
import 'package:approval/widgets/card.dart';
import 'package:approval/widgets/loader.dart';
import 'package:flutter/material.dart';

// import 'package:provider/provider.dart';

class AccInfoScreen extends StatefulWidget {
  final String accNumber;
  AccInfoScreen(this.accNumber);

  @override
  _AccInfoScreenState createState() => _AccInfoScreenState();
}

class _AccInfoScreenState extends State<AccInfoScreen> {
  Future<AccountInfo> accIinfo;
  @override
  void initState() {
    super.initState();
    accIinfo = accInfo(widget.accNumber);
  }

  @override
  Widget build(BuildContext cont) {
    // final userData = Provider.of<User>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text("Account Info"),
      ),
      body: FutureBuilder(
          future: accIinfo,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.connectionState == ConnectionState.none ||
                snapshot.connectionState == ConnectionState.active ||
                snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: loading());
            } else if (snapshot.connectionState == ConnectionState.done) {
              if (snapshot.hasError) {
                return Text("Internal Server Error");
              } else if (snapshot.data == null) {
                return Center(
                  child: Text("Null"),
                );
              } else if (snapshot.data != null) {
                AccountInfo resp = snapshot.data;

                if (resp == null) {
                  return Center(
                    child: Text("No data Found"),
                  );
                } else {
                  return Container(
                    decoration: BoxDecoration(
                        // image: new DecorationImage(
                        //     image: new AssetImage("assets/images/newbk.png"),
                        //     fit: BoxFit.cover),
                        // color: Colors.white,
                        ),
                    child: Column(children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ListView(
                            children: <Widget>[
                              Fcard(
                                "Account Number",
                                resp.accountNumber,
                                20,
                                Colors.transparent,
                              ),
                              Fcard(
                                "Account Name",
                                resp.accountName,
                                20,
                                Colors.grey[200],
                              ),
                              Fcard(
                                "Product",
                                resp.balance.product,
                                20,
                                Colors.transparent,
                              ),
                              Fcard(
                                "Risk Code",
                                resp.customerName,
                                20,
                                Colors.grey[200],
                              ),
                              Fcard(
                                "Account Branch ",
                                resp.balance.brCode,
                                20,
                                Colors.transparent,
                              ),
                              Fcard(
                                "Book Amount",
                                resp.currency + " " + resp.balance.bkBalance,
                                20,
                                Colors.grey[200],
                              ),
                              Fcard(
                                "Available Balance",
                                resp.currency + " " + resp.balance.avBalance,
                                20,
                                Colors.transparent,
                              ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                          height: 45,
                          width: MediaQuery.of(context).size.width / 1.2,
                          decoration: BoxDecoration(
                            // gradient: getCustomgradient(),
                            color: Colors.lightBlueAccent,
                            borderRadius: BorderRadius.all(Radius.circular(50)),
                          ),
                          child: FlatButton(
                            onPressed: () {
                              Navigator.of(context).pushNamed(MandateRoute,
                                  arguments: widget.accNumber);
                            },
                            child: Text(
                              "View Mandate",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          )),
                      SizedBox(
                        height: 10,
                      )
                    ]),
                  );
                }
              } else {
                return Center(
                  child: Text("error from no where"),
                );
              }
            } else {
              return Center(
                child: Text("without snapshot error"),
              );
            }
          }),
    );
  }
}
