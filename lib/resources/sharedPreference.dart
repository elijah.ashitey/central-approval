/*import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:shared_preferences/shared_preferences.dart';

Future<bool> saveDataN(String username) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return await preferences.setString("name", username);
}

Future<bool> saveDataP(String pin) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return await preferences.setString("pin", pin);
}

Future<bool> saveUrl(String url) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return await preferences.setString("url", url);
}

Future<String> loadurl() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.getString("url");
}

Future<String> loadDataN() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.getString("name");
}

Future<String> loadDataP() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  return preferences.getString("pin");
}

class ImageClass {
  static const String IMG_KEY = 'IMAGE_KEY';

  static Future<bool> saveImage(String value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return await preferences.setString(IMG_KEY, value);
  }

  static Future<String> getImage() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return preferences.getString(IMG_KEY);
  }

  static String base64String(Uint8List data) {
    return base64Encode(data);
  }

  static Image imageFromBase64String(
      String base64String, BuildContext context) {
    return Image.memory(
      base64Decode(base64String),
      fit: BoxFit.cover,
      width: double.maxFinite,
      height: double.maxFinite,
      // width: MediaQuery.of(context).size.width * 0.35,
      // height: MediaQuery.of(context).size.height * 0.2,
    );
  }
}
*/

import 'package:shared_preferences/shared_preferences.dart';

save(String key, dynamic value) async {
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
  if (value is bool) {
    sharedPrefs.setBool(key, value);
  } else if (value is String) {
    sharedPrefs.setString(key, value);
  } else if (value is int) {
    sharedPrefs.setInt(key, value);
  } else if (value is double) {
    sharedPrefs.setDouble(key, value);
  } else if (value is List<String>) {
    sharedPrefs.setStringList(key, value);
  }
}

Future<bool> isDataSaved() async {
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
  if (sharedPrefs.getString("username") != null &&
      sharedPrefs.getString("password") != null) {
    return true;
  } else {
    return false;
  }
}

getusername() async {
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
  String username = sharedPrefs.getString("username");
  return username;
}

getpassword() async {
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
  String username = sharedPrefs.getString("password");
  return username;
}
