import 'package:approval/models/account_info.dart';
import 'package:approval/models/base64.dart';
import 'package:approval/models/detailsModel.dart';
import 'package:approval/models/loginModel.dart';
import 'package:approval/screens/reject.dart';
import 'package:flutter/material.dart';

import 'apiCalls.dart';

class ProvAccInfo with ChangeNotifier {
  AccountInfo _accountInfo;
  AccountInfo get getInfo => _accountInfo;
  setInfo(AccountInfo info) {
    _accountInfo = info;
    notifyListeners();
  }
}

class Default with ChangeNotifier {
  bool _isloading = false;
  bool get getIsLoadind => _isloading;
  setLoading(bool load) {
    _isloading = load;
    notifyListeners();
  }
}

class Base64 with ChangeNotifier {
  Base64Api _result;

  Base64Api get getbase64 => _result;

  setbase64(Base64Api res) {
    _result = res;
    notifyListeners();
  }
}

class User with ChangeNotifier {
  String _savedName;
  bool _isBiometric = false;

  Login _user;
  int _pastPending;
  List<UserAccessList> _list;
  String _error;
  bool _notify = false;
  int _currentPending = 0;
  List<UserAccessList> get getList => _list;
  int get getPending => _pastPending;
  int get getCurrentPending => _currentPending;
  String get geterror => _error;
  String get getSavedName => _savedName;
  Login get getuser => _user;
  bool get getnotification => _notify;
  bool get getBio => _isBiometric;

  setnotification(bool mess) {
    _notify = mess;
    notifyListeners();
  }

  setBioMetric(bool mess) {
    _isBiometric = mess;
    notifyListeners();
  }

  setSavedName(String mess) {
    _savedName = mess;
    notifyListeners();
  }

  seterror(String mess) {
    _error = mess;
    notifyListeners();
  }

  setuser(Login res) {
    _user = res;
    notifyListeners();
  }

  setPending(int res) {
    _pastPending = res;
    notifyListeners();
  }

  setList(List<UserAccessList> list) {
    _list = list;
    notifyListeners();
  }

  int numOfApprovals() {
    var list = getList;
    int pending = 0;

    if (list != null) {
      for (var items in list) {
        pending = pending + int.parse(items.numApprovals);
      }
      _currentPending = pending;
      notifyListeners();
    }
    return pending;
  }

/*
  Future<void> refreshList(name, pin) async {
    // String name = await loadDataN();
    // String pin = await loadDataP();
    await Future.delayed(Duration(seconds: 2));
    // print(name);
    // print(pin);
    Login user = await login(name, pin);
    if (user != null) {
      // if (user.userAccessList != null) {}
      /*
      if (getuser.userAccessList.length < user.userAccessList.length ||
          int.parse(getuser.userAccessList[0].numApprovals) <
              int.parse(user.userAccessList[0].numApprovals)) {
        setnotification(true);
      } else {
        setnotification(false);
      }*/
      setuser(user);
    } else {
      seterror("please Check your internet or login again");
    }
  }
*/
  Future<List<UserAccessList>> refresh() async {
    String name = getuser.globalUsername;
    String branch = getuser.globalSubBra;
    await Future.delayed(Duration(seconds: 2));
    var result = await refreshData(name, branch);
    // int pastPending = getPending;
    if (result != null) {
      // int newpending = 0;
      // for (var items in result) {
      //   newpending = newpending + int.parse(items.numApprovals);
      //   _currentPending = newpending;
      //   notifyListeners();
      // }
      /*  if (newpending > pastPending) {
        setPending(newpending);

        setnotification(true);
      } else {
        setPending(newpending);

        setnotification(false);
      }*/
      setList(result);
      notifyListeners();
      return result;
    } else {
      return null;
    }
  }
}

class TransactionDetails with ChangeNotifier {
  Info _details;

  Info get getDetail => _details;

  setDetails(Info res) {
    _details = res;
    notifyListeners();
  }
}

class RejectReasonList with ChangeNotifier {
  List<RejectReason> _details;

  List<RejectReason> get getReasons => _details;

  setReasons(List<RejectReason> res) {
    _details = res;
    notifyListeners();
  }
}

class RejectReasonProvider with ChangeNotifier {
  String _comment;
  int _code;

  String get getComment => _comment;
  int get getCode => _code;

  setComment(String res) {
    _comment = res;
    notifyListeners();
  }

  setCode(int res) {
    _code = res;
    notifyListeners();
  }
}

class ThemeState with ChangeNotifier {
  ThemeData darkMode = ThemeData.dark();
  ThemeData normalMode = ThemeData.light();
  ThemeData _theme;
  bool isDark = false;
  ThemeState() {
    _theme = normalMode;
  }
  ThemeData getTheme() => _theme;
  setTheme() {
    if (!isDark) {
      _theme = darkMode;
    } else {
      _theme = normalMode;
    }
    isDark = !isDark;
    notifyListeners();
  }
}

class SharedPreferenceData with ChangeNotifier {
  String _name;
  String get getname => _name;
  setname(String name) {
    _name = name;

    notifyListeners();
  }
}
