import 'dart:convert';
import 'package:approval/models/account_info.dart';
import 'package:approval/models/approvModel.dart';
import 'package:approval/models/base64.dart';
import 'package:approval/models/detailsModel.dart';
import 'package:approval/models/loginModel.dart';
import 'package:approval/models/mandateModel.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;

import 'constants.dart';

Future<Login> login(String username, String password) async {
  final url = "${Constants.url}login?username=$username&password=$password";

  final response = await http.post(url);
  print(response.body);
  if (response.statusCode == 200) {
    Login responds = Login.fromJson(json.decode(response.body));
    if (responds.loginCode == "000") {
      return responds;
    } else {
      throw PlatformException(
        code: "API Error",
        message: responds.mess,
      );
    }
  } else {
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}

Future<AccountInfo> accInfo(String accNum) async {
  final url = "${Constants.url}balInfo/$accNum";

  final response = await http.get(url);
  print(response.body);

  if (response.statusCode == 200) {
    AccountInfo responds = AccountInfo.fromJson(json.decode(response.body));

    return responds;
  } else {
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}

Future<List<Info>> transDetails(String itemCode, String username) async {
  final url = "${Constants.url}details?itemCode=$itemCode&username=$username";

  final response = await http.get(url);
  print(response.body);
  if (response.statusCode == 200) {
    return infoFromJson(response.body);
  } else {
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}

Future<Approve> approvAPI(String username, String transtype, String approve,
    String reason, String transID) async {
  final url =
      "${Constants.url}approvetrans?username=$username&transtype=$transtype&approveflag=$approve&reason=$reason&transId=$transID";

  final response = await http.post(url);
  print(response.body);

  if (response.statusCode == 200) {
    Approve responds = Approve.fromJson(json.decode(response.body));

    return responds;
  } else {
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}

Future<List<UserAccessList>> refreshData(String username, String branch) async {
  final url = "${Constants.url}refresh?username=$username&branch=$branch";

  final response = await http.get(url);
  print(response.body);
  if (response.statusCode == 200) {
    return userAccessListFromJson(response.body);
  } else {
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}

Future<MandateModel> fetchMandate(String accNum) async {
  final url = "${Constants.url}mandate/$accNum";

  final response = await http.get(url);
  print(response.body);

  if (response.statusCode == 200) {
    MandateModel responds = MandateModel.fromJson(json.decode(response.body));
    return responds;
  } else {
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}

Future<Base64Api> readBase64(String batchNo) async {
  final url = "${Constants.url}base64?batchNo=$batchNo";

  final response = await http.get(url);
  print(response.body);

  if (response.statusCode == 200) {
    // print(infoFromJson(response.body));
    // print(response.body);
    // print(welcomeFromJson(response.body));
    // base64ApiFromJson(response.body).z((x) => print(x.backImgBase64));

    List<Base64Api> list = base64ApiFromJson(response.body);
    Base64Api one = list[0];
    // print(one.imgBase64);
    // print(one.pdfBase64);
    // print(base64ApiFromJson(response.body));
    return one;
  } else {
    throw PlatformException(
      code: response.statusCode.toString(),
      message: "Error connecting to server",
    );
  }
}
