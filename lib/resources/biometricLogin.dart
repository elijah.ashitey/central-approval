import 'package:approval/widgets/dialogs.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:local_auth/local_auth.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../routing_constants.dart';
import 'apiCalls.dart';
import 'connectionChecker.dart';
import 'provider.dart';

Future<bool> authenticate() async {
  final _auth = LocalAuthentication();

  bool isAuthenticated;
  try {
    isAuthenticated = await _auth.authenticateWithBiometrics(
      localizedReason: 'authenticate to access',
      useErrorDialogs: true,
      stickyAuth: true,
    );
    // print(isAuthenticated);
  } on PlatformException catch (e) {
    print(e);
  }
  return isAuthenticated;
}

biologin(BuildContext context) async {
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
  final userData = Provider.of<User>(context, listen: false);
  final defaults = Provider.of<Default>(context, listen: false);

  final res = await authenticate();
  if (res) {
    String username = sharedPrefs.getString("username");
    String password = sharedPrefs.getString("password");
    print(username);
    print(password);
    try {
      defaults.setLoading(true);
      final checkinternet = await internetCheck();
      if (checkinternet) {
        // print(username.text);
        // print(password.text);
        final res = await login(username, password);
        userData.setuser(res);

        Navigator.pushNamed(context, HomePageRoute);
      } else {
        info(context, "No Internet Access");
      }
    } on PlatformException catch (e) {
      print("Error:${e.message}");
      err(context, e.message.toString());
    } finally {
      defaults.setLoading(false);
    }
  }
}

rememberMelogin(BuildContext context, String password) async {
  final SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
  final userData = Provider.of<User>(context, listen: false);
  final defaults = Provider.of<Default>(context, listen: false);

  try {
    String username = sharedPrefs.getString("username");

    defaults.setLoading(true);
    final checkinternet = await internetCheck();
    if (checkinternet) {
      // print(username.text);
      // print(password.text);
      final res = await login(username, password);
      userData.setuser(res);

      Navigator.pushNamed(context, HomePageRoute);
    } else {
      info(context, "No Internet Access");
    }
  } on PlatformException catch (e) {
    print("Error:${e.message}");
    err(context, e.message.toString());
  } finally {
    defaults.setLoading(false);
  }
}

Future<bool> isBiometricCheck() async {
  final _auth = LocalAuthentication();

  bool canCheckBiometrics = await _auth.canCheckBiometrics;
  return canCheckBiometrics;
}
